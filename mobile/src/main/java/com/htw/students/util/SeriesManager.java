package com.htw.students.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by adair on 28.01.2016.
 */
public class SeriesManager {

    private static SeriesManager mDefaultManager = null;

    protected Map<String, SeriesBundle> mSeriesBundles = new HashMap<>();

    public static SeriesManager defaultManager() {
        if (mDefaultManager == null) {
            mDefaultManager = new SeriesManager();
        }
        return mDefaultManager;
    }

    public SeriesManager() {}

    public SeriesBundle getSeriesBundle(String id) {
        return mSeriesBundles.get(id);
    }

    public SeriesBundle getAddSeriesBundle(String id) {
        SeriesBundle sb = mSeriesBundles.get(id);
        if (sb == null) {
            sb = new SeriesBundle();
            mSeriesBundles.put(id, sb);
        }
        return sb;
    }

    public SeriesBundle removeSeriesBundle(String id) {
        return mSeriesBundles.remove(id);
    }
}
