package com.htw.students.util;

import com.htw.students.common.logger.Log;
import com.jjoe64.graphview.series.DataPoint;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by adair on 28.01.2016.
 */
public class Serialize {

    private static final String TAG = Serialize.class.getSimpleName();

    public static String serialize(List<DataPoint> list) {
        DataPoint dp;
        DecimalFormat format = new DecimalFormat("0.00");
        StringBuilder sb = new StringBuilder();
        for (int i = 0, size = list.size(); i < size; i++) {
            dp = list.get(i);
            sb.append((int) dp.getX());
            sb.append(",");
            sb.append(format.format(dp.getY()));
            sb.append(";");
        }
        if (sb.length() > 0)
            sb.setLength(sb.length()-1);
        return sb.toString();
    }

    public static List<DataPoint> deserialize(String string) {
        List<DataPoint> list = new ArrayList<>();
        if (string != null && !string.isEmpty()) {
            try {
                String[] parts2, parts = string.split(";");
                for (String s : parts) {
                    parts2 = s.split(",");
                    list.add(new DataPoint(Integer.parseInt(parts2[0]), Double.parseDouble(parts2[1])));
                }
            } catch (Exception ex) {
                Log.e(TAG, "Unexpected syntax");
            }
        }
        return list;
    }

}
