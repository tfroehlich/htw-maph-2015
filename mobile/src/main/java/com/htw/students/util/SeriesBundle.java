package com.htw.students.util;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Timm Fröhlich on 28.01.2016.
 *
 * Public container class for data series.
 *
 * Pre-configured for a time graph.
 */
public class SeriesBundle {

    public LineGraphSeries<DataPoint> mSeries = new LineGraphSeries<>();
    public List<DataPoint> mSeriesData = new ArrayList<>();
    public ArrayDeque<DataPoint> mUpdateData = new ArrayDeque<>();

    public int mSeriesMaxSize = 10000;
    public int mLastUpdateIndex = 0;
    public long mStart = 0;
    public long mMaxTimeSpan = 20 * 1000; // in ms

    public double minY = 0.0;
    public double maxY = 100.0;
    public double minX = -mMaxTimeSpan;
    public double maxX = 0.0;

    public boolean xAxisBoundsManual = true;
    public boolean yAxisBoundsManual = true;
    public boolean yAxisBoundsManualUpdate = yAxisBoundsManual && false;

    public boolean randomSeriesWereRunning = false;

    public long mLastExport = 0;
    public int mLastExportIndex = 0;

    public SeriesBundle() {
        resetData();
    }

    public void resetData() {
        mSeries.resetData(new DataPoint[]{new DataPoint(0, 0)});
        mUpdateData.clear();
        mSeriesData.clear();
        mLastUpdateIndex = 0;
        mLastExport = 0;
        mLastExportIndex = 0;
    }

    public void updateValues(GraphView gv) {
        Viewport vp = gv.getViewport();
        minY = vp.getMinY(false);
        maxY = vp.getMaxY(false);
        minX = vp.getMinX(false);
        maxX = vp.getMaxX(false);
    }
}
