package com.htw.students.bluetooth;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import com.htw.students.common.logger.Log;

/**
 * Created by Patrick
 */
public class BluetoothService extends Service {

    private static final String TAG = BluetoothService.class.getSimpleName();

    private static final UUID BT_MODULE_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
//    private static final String BT_MAC_ADDRESS = "00:12:05:08:80:06";

    private static final int STATE_NONE = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;
    private static int BT_STATE = STATE_NONE;

    private BluetoothAdapter btAdapter = null;
    private ConnectThread btConnectThread;
    private static ConnectedThread btConnectedThread;

    private String mMacAddress = null;
    private boolean mMacAddressValid = false;

    public String getConnectedMacAddress(boolean onlyWhenValid) {
        Log.d(TAG, mMacAddress);
        if (onlyWhenValid) {
            return mMacAddressValid ? mMacAddress : null;
        }
        return mMacAddress;
    }

    private final IBinder lBinder = new LocalBinder();
    private Handler mHandler = null;

    public class LocalBinder extends Binder {
        public BluetoothService getService() {
            return BluetoothService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "");
        return lBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "");
        super.onCreate();
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        if (btAdapter != null) {
//            connectToDevice("00:12:05:08:80:06");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        closeBtConnection();
    }

    public void registerHandler(Handler serviceHandler) {
        mHandler = serviceHandler;
    }

    public synchronized void connectToDevice(String macAddress) {
        Log.d(TAG, "");
//        for (BluetoothDevice d : btAdapter.getBondedDevices()) {
//            Log.d(TAG, "-" + d.getAddress());
//        }

        BluetoothDevice device = btAdapter.getRemoteDevice(macAddress);
        mMacAddressValid = false;
        mMacAddress = macAddress;
        Log.d(TAG, "device: " + (device != null ? device.getAddress() : "no device"));

        if (BT_STATE == STATE_CONNECTING) {
            if (btConnectThread != null) {
                btConnectThread.cancel();
                btConnectThread = null;
            }
        }

        if (btConnectedThread != null) {
            btConnectedThread.cancel();
            btConnectedThread = null;
        }

        btConnectThread = new ConnectThread(device);
        btConnectThread.start();
        BT_STATE = STATE_CONNECTING;
    }

    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;

        public ConnectThread(BluetoothDevice device) {
            BluetoothSocket tmp = null;
            try {
                tmp = device.createRfcommSocketToServiceRecord(BT_MODULE_UUID);
            } catch (IOException e) {
                e.printStackTrace();
            }
            mmSocket = tmp;
        }

        @Override
        public void run() {
            setName("ConnectThread");
            btAdapter.cancelDiscovery();
            try {
                mmSocket.connect();
            } catch (IOException e) {
                try {
                    mmSocket.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                return;

            }
            synchronized (BluetoothService.this) {
                btConnectThread = null;
            }
            connected(mmSocket);
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect socket failed", e);
            }
        }
    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private StringBuilder recDataString = new StringBuilder();

        public ConnectedThread(BluetoothSocket socket) {
            Log.d(TAG, "");
            mmSocket = socket;
            InputStream tmpIn = null;
            try {
                tmpIn = socket.getInputStream();
            } catch (IOException e) {
                Log.e(TAG, "temp sockets not mCreated", e);
            }
            mmInStream = tmpIn;
        }

        @Override
        public void run() {
            Log.d(TAG, "");
            byte[] buffer = new byte[256];
            int bytes;

            while (true) {
                try {
                    bytes = mmInStream.read(buffer);
                    if (mHandler != null) {
                        mHandler.obtainMessage(Constants.MESSAGE_READ, bytes, -1, buffer).sendToTarget();
                    }
                } catch (IOException e) {
                    Log.e(TAG, e.getMessage());
                    break;
                }
            }
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect socket failed", e);
            }
        }

    }

    private synchronized void connected(BluetoothSocket mmSocket) {
        Log.d(TAG, "");
        mMacAddressValid = true;

        if (btConnectThread != null) {
            btConnectThread.cancel();
            btConnectThread = null;
        }
        if (btConnectedThread != null) {
            btConnectedThread.cancel();
            btConnectedThread = null;
        }
        btConnectedThread = new ConnectedThread(mmSocket);
        btConnectedThread.start();
        BT_STATE = STATE_CONNECTED;
    }

    public void closeBtConnection() {
        Log.d(TAG, "");

        mMacAddressValid = false;

        if (btConnectThread != null) {
            btConnectThread.cancel();
            btConnectThread = null;
        }
        if (btConnectedThread != null) {
            btConnectedThread.cancel();
            btConnectedThread = null;
        }

        BT_STATE = STATE_NONE;
    }

}
