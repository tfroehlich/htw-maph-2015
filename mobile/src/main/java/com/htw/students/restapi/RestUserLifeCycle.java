package com.htw.students.restapi;

import android.content.Context;

import com.htw.students.common.logger.Log;
import com.htw.students.model.Measurement;
import com.htw.students.model.User;
import com.jjoe64.graphview.series.DataPoint;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Timm Fröhlich on 31.01.2016.
 */
public class RestUserLifeCycle {

    private static final String TAG = RestUserLifeCycle.class.getSimpleName();

    protected Context mContext;
    protected static boolean mTestMeasurements;
    protected static User mLastUser;
    protected static Measurement mLastMeasurement;


    //================================================================================
    // construction
    //================================================================================

    public RestUserLifeCycle(Context context) {
        mContext = context;
    }

    private static RestUserLifeCycle INSTANCE = null;

    public static RestUserLifeCycle instance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new RestUserLifeCycle(context);
        }
        return INSTANCE;
    }

    //================================================================================
    // methods
    //================================================================================

    public void start(boolean testMeasurements) {
        Log.d(TAG, "");
        mTestMeasurements = testMeasurements;
        createUser();
    }

    public void finish() {
        Log.d(TAG, "Finished");
    }

    //================================================================================
    // - User life cycle
    //================================================================================

    public void createUser() {
        String name = "";
        RestClientUsage.instance().user_add(User.TYPE.PATIENT, name, new ResultHandler() {
            @Override
            public void onResult(Object obj, boolean success) {
                if (success) {
                    User user = (User) obj;
                    if (user != null) {
                        Log.d(TAG, "createPatient user: " + user);
                        getUser(user.mId);
                    } else {
                        Log.w(TAG, "createPatient: : null: " + user);
                    }
                } else {
                    Log.w(TAG, "createPatient failed");
                }
            }
        });
    }

    public void getUser(String userId) {
        RestClientUsage.instance().user_get(userId, new ResultHandler() {
            @Override
            public void onResult(Object obj, boolean success) {
                if (success) {
                    User user = (User) obj;
                    if (user != null) {
                        Log.d(TAG, "getPatient user: " + user);
                        mLastUser = user;

                        if (mTestMeasurements) {
                            createMeasurement();
                        } else {
                            deleteUser(mLastUser.mId);
                        }

                    } else {
                        Log.w(TAG, "getPatient: : null: " + user);
                    }
                } else {
                    Log.w(TAG, "getPatient failed");
                }
            }
        });
    }

    public void deleteUser(String userId) {
        RestClientUsage.instance().user_delete(userId, new ResultHandler() {
            @Override
            public void onResult(Object obj, boolean success) {
                if (success) {
                    User user = (User) obj;
                    if (user == null) {
                        Log.d(TAG, "deletePatient: " + user);
                        deletePatient2(mLastUser.mId);
                    } else {
                        Log.w(TAG, "deletePatient: : not null: " + user);
                    }
                } else {
                    Log.w(TAG, "deletePatient failed");
                }
            }
        });
    }

    public void deletePatient2(String userId) {
        RestClientUsage.instance().user_delete(userId, new ResultHandler() {
            @Override
            public void onResult(Object obj, boolean success) {
                if (success) {
                    User user = (User) obj;
                    if (user == null) {
                        Log.d(TAG, "deletePatient2: " + user);
                        getPatient2(mLastUser.mId);
                    } else {
                        Log.w(TAG, "deletePatient2: : not null: " + user);
                    }
                } else {
                    Log.w(TAG, "deletePatient2 failed");
                }
            }
        });
    }

    public void getPatient2(String userId) {
        RestClientUsage.instance().user_get(userId, new ResultHandler() {
            @Override
            public void onResult(Object obj, boolean success) {
                if (success) {
                    User user = (User) obj;
                    if (user == null) {
                        Log.d(TAG, "getPatient2: " + user);
                        finish();
                    } else {
                        Log.w(TAG, "getPatient2: not null: " + user);
                    }
                } else {
                    Log.w(TAG, "getPatient failed");
                }
            }
        });
    }

    //================================================================================
    // - Measurement life cycle
    //================================================================================

    public void createMeasurement() {
        List<DataPoint> list = new ArrayList<>();
        for (int i=1; i<4; i++)
            list.add(new DataPoint(i*100, i*11.1));
        createMeasurement(mLastUser.mId, 123, list);
    }

    public void createMeasurement(String userId, int groupId, List<DataPoint> values) {
        RestClientUsage.instance().measurement_add(userId, groupId, values, new ResultHandler() {
            @Override
            public void onResult(Object obj, boolean success) {
                if (success) {
                    Measurement measurement = (Measurement) obj;
                    if (measurement != null) {
                        Log.d(TAG, "createMeasurement measurement: " + measurement);
                        getMeasurement(measurement.mId);
                    } else {
                        Log.w(TAG, "createMeasurement: : null: " + measurement);
                    }
                } else {
                    Log.w(TAG, "createMeasurement failed");
                }
            }
        });
    }

    public void getMeasurement(int measurementId) {
        int index = 0;
        RestClientUsage.instance().measurement_get(measurementId, index, new ResultHandler() {
            @Override
            public void onResult(Object obj, boolean success) {
                if (success) {
                    Measurement measurement = (Measurement) obj;
                    if (measurement != null) {
                        Log.d(TAG, "getMeasurement measurement: " + measurement);
                        mLastMeasurement = measurement;
                        appendMeasurement(mLastMeasurement.mMeasurementId);
                    } else {
                        Log.w(TAG, "getMeasurement: : null: " + measurement);
                    }
                } else {
                    Log.w(TAG, "getMeasurement failed");
                }
            }
        });
    }

    public void appendMeasurement(int measurementId) {
        List<DataPoint> list = new ArrayList<>();
        for (int i=5; i<9; i++)
            list.add(new DataPoint(i*100, i*11.1));
        appendMeasurement(measurementId, list);
    }

    public void appendMeasurement(int measurementId, List<DataPoint> list) {
        int index = 0;
        RestClientUsage.instance().measurement_append(measurementId, list, new ResultHandler() {
            @Override
            public void onResult(Object obj, boolean success) {
                if (success) {
                    if (obj != null) {
                        Log.d(TAG, "appendMeasurement obj: " + obj);
                        getMeasurement2(mLastMeasurement.mMeasurementId);
                    } else {
                        Log.w(TAG, "appendMeasurement: : null: " + obj);
                    }
                } else {
                    Log.w(TAG, "appendMeasurement failed");
                }
            }
        });
    }

    public void getMeasurement2(int measurementId) {
        int index = 0;
        RestClientUsage.instance().measurement_get(measurementId, index, new ResultHandler() {
            @Override
            public void onResult(Object obj, boolean success) {
                if (success) {
                    Measurement measurement = (Measurement) obj;
                    if (measurement != null) {
                        Log.d(TAG, "getMeasurement2 measurement: " + measurement);
                        mLastMeasurement = measurement;
                        getUserMeasurementList(mLastUser.mId);
                    } else {
                        Log.w(TAG, "getMeasurement2: : null: " + measurement);
                    }
                } else {
                    Log.w(TAG, "getMeasurement2 failed");
                }
            }
        });
    }

    public void getUserMeasurementList(String userId) {
        RestClientUsage.instance().measurement_user_list_get(userId, new ResultHandler() {
            @Override
            public void onResult(Object obj, boolean success) {
                if (success) {
                    List<Integer> list = (List<Integer>) obj;
                    if (list != null) {
                        Log.d(TAG, "getUserMeasurementList measurement: " + list);
                        getGroupMeasurementList(mLastMeasurement.mGroupId);
                    } else {
                        Log.w(TAG, "getUserMeasurementList: : null: " + list);
                    }
                } else {
                    Log.w(TAG, "getUserMeasurementList failed");
                }
            }
        });
    }

    public void getGroupMeasurementList(int groupId) {
        RestClientUsage.instance().measurement_group_list_get(groupId, new ResultHandler() {
            @Override
            public void onResult(Object obj, boolean success) {
                if (success) {
                    List<Integer> list = (List<Integer>) obj;
                    if (list != null) {
                        Log.d(TAG, "getGroupMeasurementList measurement: " + list);
                        deleteMeasurement(mLastMeasurement.mMeasurementId);
                    } else {
                        Log.w(TAG, "getGroupMeasurementList: : null: " + list);
                    }
                } else {
                    Log.w(TAG, "getGroupMeasurementList failed");
                }
            }
        });
    }

    public void deleteMeasurement(int measurementId) {
        RestClientUsage.instance().measurement_delete(measurementId, new ResultHandler() {
            @Override
            public void onResult(Object obj, boolean success) {
                if (success) {
                    Measurement measurement = (Measurement) obj;
                    if (measurement == null) {
                        Log.d(TAG, "deleteMeasurement: " + measurement);
                        getMeasurement3(mLastMeasurement.mMeasurementId);
                    } else {
                        Log.w(TAG, "deleteMeasurement: : not null: " + measurement);
                    }
                } else {
                    Log.w(TAG, "deleteMeasurement failed");
                }
            }
        });
    }

    public void getMeasurement3(int measurementId) {
        int index = 0;
        RestClientUsage.instance().measurement_get(measurementId, index, new ResultHandler() {
            @Override
            public void onResult(Object obj, boolean success) {
                if (success) {
                    Measurement measurement = (Measurement) obj;
                    if (measurement == null) {
                        Log.d(TAG, "getMeasurement3: " + measurement);
                        deleteUser(mLastUser.mId);
                    } else {
                        Log.w(TAG, "getMeasurement3: not null: " + measurement);
                    }
                } else {
                    Log.w(TAG, "getMeasurement3 failed");
                }
            }
        });
    }
}
