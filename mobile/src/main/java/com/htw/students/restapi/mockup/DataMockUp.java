package com.htw.students.restapi.mockup;

import com.htw.students.common.logger.Log;
import com.htw.students.model.Measurement;
import com.htw.students.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Timm Fröhlich on 31.01.2016.
 */
public class DataMockUp {

    private static final String TAG = DataMockUp.class.getSimpleName();

    public List<User> users = new ArrayList<>();

    public List<Measurement> measurements = new ArrayList<>();

    private static DataMockUp INSTANCE = null;

    public static DataMockUp instance() {
        if (INSTANCE == null) {
            INSTANCE = new DataMockUp();
        }
        return INSTANCE;
    }

    //================================================================================
    // Util
    //================================================================================

    public static List<String> RAND_NAMES = new ArrayList<String>() {{
        add("hopeful sailor");
        add("crummy hygienist");
        add("euphoric acrobat");
        add("sensitive cobbler");
        add("mellow carpenter");
        add("miserable manager");
        add("dopey designer");
        add("enchanted robber");
        add("peaceful cameraman");
        add("ardent broker");
        add("agitated cashier");
        add("fulfilled reporter");
        add("interest messenger");
        add("resigned publisher");
        add("empathic paver");
        add("gleeful listener");
        add("goofy researcher");
        add("satisfied physicist");
        add("alert dispatcher");
        add("yearning engineer");
        add("thrilled nun");
        add("courageous caterer");
        add("grouchy salesman");
        add("delirious actor");
        add("puzzled worshipper");
    }};

    public static String randName() {
        int rand = new Random().nextInt(RAND_NAMES.size()); // % (-1);
        Log.d(TAG, "RAND_NAMES size: " + RAND_NAMES.size() + ", rand: " + rand);
        return RAND_NAMES.get(rand);
    }
}
