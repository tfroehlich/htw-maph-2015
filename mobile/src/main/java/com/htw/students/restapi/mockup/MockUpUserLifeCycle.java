package com.htw.students.restapi.mockup;

import android.content.Context;

import com.htw.students.common.logger.Log;
import com.htw.students.model.User;
import com.htw.students.restapi.ResultHandler;

/**
 * Created by Timm Fröhlich on 31.01.2016.
 */
public class MockUpUserLifeCycle {

    private static final String TAG = MockUpUserLifeCycle.class.getSimpleName();

    protected Context mContext;

    public MockUpUserLifeCycle(Context context) {
        mContext = context;
    }

    private static MockUpUserLifeCycle INSTANCE = null;

    public static MockUpUserLifeCycle instance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new MockUpUserLifeCycle(context);
        }
        return INSTANCE;
    }

    public void start() {
        createPatient();
    }

    public void createPatient() {
        String name = "";
        RestClientMockUp.instance().user_add(User.TYPE.PATIENT, name, new ResultHandler() {
            @Override
            public void onResult(Object obj, boolean success) {
                if (success) {
                    User user = (User) obj;
                    Log.d(TAG, "createPatient user: " + user);
                    getPatient(user.mId);
                } else {
                    Log.d(TAG, "createPatient failed");
                }
            }
        });
    }

    public void getPatient(String userId) {
        RestClientMockUp.instance().user_get(userId, new ResultHandler() {
            @Override
            public void onResult(Object obj, boolean success) {
                if (success) {
                    User user = (User) obj;
                    Log.d(TAG, "getPatient user: " + user);
                    deletePatient(user.mId);
                } else {
                    Log.d(TAG, "getPatient failed");
                }
            }
        });
    }

    public void deletePatient(String userId) {
        RestClientMockUp.instance().user_delete(userId, new ResultHandler() {
            @Override
            public void onResult(Object obj, boolean success) {
                if (success) {
                    User user = (User) obj;
                    Log.d(TAG, "deletePatient: " + user);
                    deletePatient2(user.mId);
                } else {
                    Log.d(TAG, "deletePatient failed");
                }
            }
        });
    }

    public void deletePatient2(String userId) {
        RestClientMockUp.instance().user_delete(userId, new ResultHandler() {
            @Override
            public void onResult(Object obj, boolean success) {
                if (success) {
                    User user = (User) obj;
                    Log.d(TAG, "deletePatient2: " + user);
                    finish();
                } else {
                    Log.d(TAG, "deletePatient2 failed");
                }
            }
        });
    }

    public void finish() {
        Log.d(TAG, "Finished");
    }
}
