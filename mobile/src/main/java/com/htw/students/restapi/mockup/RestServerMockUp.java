package com.htw.students.restapi.mockup;

import com.htw.students.model.Measurement;
import com.htw.students.model.User;
import com.jjoe64.graphview.series.DataPoint;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

/**
 * Created by Timm Fröhlich on 31.01.2016.
 */
public class RestServerMockUp {

    private static RestServerMockUp INSTANCE = null;

    public static RestServerMockUp instance() {
        if (INSTANCE == null) {
            INSTANCE = new RestServerMockUp();
        }
        return INSTANCE;
    }

    //================================================================================
    // User requests
    //================================================================================

    public User user_add(User.TYPE type, String name) {
        User user = new User();
        user.mId = UUID.randomUUID().toString();
        user.mName = name == null || name.isEmpty() ? DataMockUp.randName() : name;
        user.mType = type;
        DataMockUp.instance().users.add(user);
        return user;
    }

    public User user_get(String id) {
        for (User u : DataMockUp.instance().users)
            if (u.mId.equals(id))
                return u;
        return null;
    }

    public User user_delete(String id) {
        User user;
        Iterator<User> itr;
        for (itr = DataMockUp.instance().users.iterator(); itr.hasNext(); ) {
            user = itr.next();
            if (user.mId.equals(id)) {
                itr.remove();
                return user;
            }
        }
        return null;
    }

    //================================================================================
    // Measurement requests
    //================================================================================

    public Measurement measurement_add(String userId, int measurementGroupId, List<DataPoint> values) {
        Measurement measurement = new Measurement();
        measurement.mId = DataMockUp.instance().measurements.size()+1;
        measurement.mGroupId = measurementGroupId;
        measurement.mUserId = userId;
        measurement.mValues = values;
        measurement.mModified =
        measurement.mCreated = new Date();
        DataMockUp.instance().measurements.add(measurement);
        return measurement;
    }

    public boolean measurement_append(int id, List<DataPoint> values) {
        Measurement m = measurement_get(id);
        if (m != null) {
            m.mValues.addAll(values);
            return true;
        }
        return false;
    }

    public Measurement measurement_get(int id) {
        for (Measurement m : DataMockUp.instance().measurements)
            if (m.mId == id)
                return m;
        return null;
    }

    public Measurement measurement_delete(int id) {
        Measurement measurement;
        Iterator<Measurement> itr;
        for (itr = DataMockUp.instance().measurements.iterator(); itr.hasNext(); ) {
            measurement = itr.next();
            if (measurement.mId == id) {
                itr.remove();
                return measurement;
            }
        }
        return null;
    }

    public List<Integer> measurement_user_list_get(String userId) {
        List<Integer> list = new ArrayList<>();
        for (Measurement m : DataMockUp.instance().measurements) {
            if (m.mUserId.equals(userId))
                list.add(m.mId);
        }
        return list;
    }

    public List<Integer> measurement_group_list_get(int groupId) {
        List<Integer> list = new ArrayList<>();
        for (Measurement m : DataMockUp.instance().measurements) {
            if (m.mGroupId == groupId)
                list.add(m.mId);
        }
        return list;
    }


    //================================================================================
    // Other requests
    //================================================================================

    public Integer destroyAll() {
        DataMockUp.instance().users.clear();
        DataMockUp.instance().measurements.clear();
        return 1;
    }

    public List<Integer> measurement_user_delete(String userId) {
        List<Integer> list = new ArrayList<>();
        Measurement measurement;
        Iterator<Measurement> itr;
        for (itr = DataMockUp.instance().measurements.iterator(); itr.hasNext(); ) {
            measurement = itr.next();
            if (measurement.mUserId.equals(userId)) {
                list.add(measurement.mId);
                itr.remove();
            }
        }
        return list;
    }

}
