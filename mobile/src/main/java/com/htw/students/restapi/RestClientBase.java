package com.htw.students.restapi;

import com.htw.students.model.User;
import com.jjoe64.graphview.series.DataPoint;

import java.util.List;

/**
 * Created by Timm Fröhlich on 31.01.2016.
 */
public class RestClientBase {

    private static final String TAG = RestClientBase.class.getSimpleName();

    //================================================================================
    // User requests
    //================================================================================

    public void user_add(User.TYPE type, String name, final ResultHandler rh) {
        throw new UnsupportedOperationException("Not implemented");
    }

    public void user_get(String id, final ResultHandler rh) {
        throw new UnsupportedOperationException("Not implemented");
    }

    public void user_delete(String id, final ResultHandler rh) {
        throw new UnsupportedOperationException("Not implemented");
    }

    public void user_list(final ResultHandler rh) {
        throw new UnsupportedOperationException("Not implemented");
    }

    //================================================================================
    // Measurement requests
    //================================================================================

    public void measurement_add(String userId, int measurementGroupId, List<DataPoint> values, final ResultHandler rh) {
        throw new UnsupportedOperationException("Not implemented");
    }

    public void measurement_append(int measurementId, List<DataPoint> values, final ResultHandler rh) {
        throw new UnsupportedOperationException("Not implemented");
    }

    public void measurement_get(int id, int index, final ResultHandler rh) {
        throw new UnsupportedOperationException("Not implemented");
    }

    public void measurement_delete(int id, final ResultHandler rh) {
        throw new UnsupportedOperationException("Not implemented");
    }

    public void measurement_user_list_get(String userId, final ResultHandler rh) {
        throw new UnsupportedOperationException("Not implemented");
    }

    public void measurement_group_list_get(int groupId, final ResultHandler rh) {
        throw new UnsupportedOperationException("Not implemented");
    }

    //================================================================================
    // Other requests
    //================================================================================

    public void destroyAll(final ResultHandler rh) {
        throw new UnsupportedOperationException("Not implemented");
    }
}
