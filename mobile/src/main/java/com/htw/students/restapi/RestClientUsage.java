package com.htw.students.restapi;

import com.htw.students.common.logger.Log;
import com.htw.students.model.Measurement;
import com.htw.students.model.User;
import com.htw.students.util.Serialize;
import com.jjoe64.graphview.series.DataPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Timm Fröhlich on 31.01.2016.
 */
public class RestClientUsage extends RestClientBase {

    private static final String TAG = RestClientUsage.class.getSimpleName();

    private static RestClientUsage INSTANCE = null;

    public static RestClientUsage instance() {
        if (INSTANCE == null) {
            INSTANCE = new RestClientUsage();
        }
        return INSTANCE;
    }

    //================================================================================
    // Conversion
    //================================================================================

    public static Date string2Date(String datetime) {
//        datetime = "2016-02-02 21:39:00";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return formatter.parse(datetime);
        } catch (ParseException e) {
            Log.d(TAG, e.getMessage());
        }
        return null;
    }

    public static boolean emptyResult(JSONObject response) {
        return response.has("result");
    }

    public static User json2User(JSONObject response) throws JSONException {
        if (emptyResult(response))
            return null;
        User user = new User();
        user.mId = response.getString("id");
        user.mName = response.getString("name");
        user.mType = response.getInt("type") == 1 ? User.TYPE.THERAPIST : User.TYPE.PATIENT;
        user.mDate = string2Date(response.getString("created"));
        return user;
    }

    public static Measurement json2Measurement(JSONObject response) throws JSONException {
        if (emptyResult(response))
            return null;
        Measurement measurement = new Measurement();
        measurement.mId = response.getInt("id");
        measurement.mGroupId = response.getInt("groupId");
        measurement.mUserId = response.getString("userId");
        measurement.mMeasurementId = response.getInt("measurementId");
        measurement.mIndex = response.getInt("index");
        measurement.mModified = string2Date(response.getString("modified"));
        measurement.mCreated = string2Date(response.getString("created"));
        if (response.has("values")) {
            String valuesStr = response.getString("values");
            measurement.mValues = Serialize.deserialize(valuesStr);
        }
        return measurement;
    }

    //================================================================================
    // User requests
    //================================================================================

    @Override
    public void user_add(User.TYPE type, String name, final ResultHandler rh) {
        String req = "users/add/" + (type == User.TYPE.THERAPIST ? 1 : 0) + "/" + name;
        RestRequest.get(req, null, new MaphJsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    rh.onResult(json2User(response), true);
                } catch (JSONException e) {
                    Log.d(TAG, e.getMessage());
                    rh.onResult(e.getMessage(), false);
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                rh.onResult(errorResponse, false);
            }
        });
    }

    @Override
    public void user_get(String id, final ResultHandler rh) {
//        Log.d(TAG, "users/get/" + id);
        RestRequest.get("users/get/" + id, null, new MaphJsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    rh.onResult(json2User(response), true);
                } catch (JSONException e) {
                    Log.d(TAG, e.getMessage());
                    rh.onResult(e.getMessage(), false);
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                rh.onResult(errorResponse, false);
            }
        });
    }

    @Override
    public void user_delete(String id, final ResultHandler rh) {
//        Log.d(TAG, "users/delete/" + id);
        RestRequest.get("users/delete/" + id, null, new MaphJsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                rh.onResult(null, true);
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                rh.onResult(errorResponse, false);
            }
        });
    }

    @Override
    public void user_list(final ResultHandler rh) {
        RestRequest.get("users/list", null, new MaphJsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    List<User> list = new ArrayList<>();
                    for (int i=0; i<response.length(); i++) {
                    Log.d(TAG, "" + response.get(i));
                        JSONObject obj = (JSONObject)response.get(i);
                        list.add(json2User(obj));
                    }
//                    Log.d(TAG, "" + response);
//                    Log.d(TAG, "" + response.get(0));
                    rh.onResult(list, true);
                } catch (JSONException e) {
                    Log.d(TAG, e.getMessage());
                    rh.onResult(e.getMessage(), false);
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                rh.onResult(errorResponse, false);
            }
        });
    }

    //================================================================================
    // Measurement requests
    //================================================================================

    @Override
    public void measurement_add(String userId, int measurementGroupId, List<DataPoint> values, final ResultHandler rh) {
        String valuesStr = Serialize.serialize(values);
        String req = "measurements/add/" + userId + "/" + measurementGroupId + "/" + valuesStr;
//        Log.d(TAG, req);

        RestRequest.get(req, null, new MaphJsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    rh.onResult(json2Measurement(response), true);
                } catch (JSONException e) {
                    Log.d(TAG, e.getMessage());
                    rh.onResult(e.getMessage(), false);
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                rh.onResult(errorResponse, false);
            }
        });
    }

    @Override
    public void measurement_get(int id, int index, final ResultHandler rh) {
        RestRequest.get("measurements/get/" + id + "/" + index, null, new MaphJsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    rh.onResult(json2Measurement(response), true);
                } catch (JSONException e) {
                    Log.d(TAG, e.getMessage());
                    rh.onResult(e.getMessage(), false);
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                rh.onResult(errorResponse, false);
            }
        });
    }

    @Override
    public void measurement_append(int measurementId, List<DataPoint> values, final ResultHandler rh) {
        String valuesStr = Serialize.serialize(values);
        String req = "measurements/append/" + measurementId + "/" + valuesStr;
//        Log.d(TAG, req);

        RestRequest.get(req, null, new MaphJsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    rh.onResult(json2Measurement(response), true);
                } catch (JSONException e) {
                    Log.d(TAG, e.getMessage());
                    rh.onResult(e.getMessage(), false);
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                rh.onResult(errorResponse, false);
            }
        });
    }

    @Override
    public void measurement_user_list_get(String userId, final ResultHandler rh) {
        RestRequest.get("measurements/userlist/" + userId, null, new MaphJsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                 super.onSuccess(statusCode, headers, response);

                try {
                    List<Integer> list = new ArrayList<>();
                    for (int i=0; i<response.length(); i++) {
                        list.add((Integer)response.get(i));
                    }
//                    Log.d(TAG, "" + response);
//                    Log.d(TAG, "" + response.get(0));
                    rh.onResult(list, true);
                } catch (JSONException e) {
                    Log.d(TAG, e.getMessage());
                    rh.onResult(e.getMessage(), false);
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                rh.onResult(errorResponse, false);
            }
        });
    }

    @Override
    public void measurement_group_list_get(int groupId, final ResultHandler rh) {
        RestRequest.get("measurements/grouplist/" + groupId, null, new MaphJsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    List<Integer> list = new ArrayList<>();
                    for (int i=0; i<response.length(); i++) {
                        list.add((Integer)response.get(i));
                    }
//                    Log.d(TAG, "" + response);
//                    Log.d(TAG, "" + response.get(0));
                    rh.onResult(list, true);
                } catch (JSONException e) {
                    Log.d(TAG, e.getMessage());
                    rh.onResult(e.getMessage(), false);
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                rh.onResult(errorResponse, false);
            }
        });
    }

    @Override
    public void measurement_delete(int id, final ResultHandler rh) {
//        Log.d(TAG, "measurements/delete/" + id);
        RestRequest.get("measurements/delete/" + id, null, new MaphJsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                rh.onResult(null, true);
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                rh.onResult(errorResponse, false);
            }
        });
    }

    //================================================================================
    // Other requests
    //================================================================================

    @Override
    public void destroyAll(final ResultHandler rh) {
    }

    //================================================================================
    // Test requests
    //================================================================================

    public void test_user_get(final ResultHandler handler) {
        RestRequest.get("users", null, new MaphJsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    JSONObject jsonUser = (JSONObject) response.get(0);
                    User user = new User();
                    user.mId = "" + jsonUser.getInt("id");
                    user.mName = jsonUser.getString("name");
                    handler.onResult(user, true);
                } catch (JSONException e) {
                    e.printStackTrace();
                    handler.onResult(e.getMessage(), false);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                handler.onResult(errorResponse, false);
            }
        });
    }

}
