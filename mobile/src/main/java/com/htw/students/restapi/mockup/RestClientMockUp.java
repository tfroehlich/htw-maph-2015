package com.htw.students.restapi.mockup;

import com.htw.students.restapi.MaphJsonHttpResponseHandler;
import com.htw.students.restapi.RestRequest;
import com.htw.students.restapi.RestClientBase;
import com.htw.students.restapi.ResultHandler;
import com.htw.students.model.User;
import com.jjoe64.graphview.series.DataPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Timm Fröhlich on 31.01.2016.
 */
public class RestClientMockUp extends RestClientBase {

    private static final String TAG = RestClientMockUp.class.getSimpleName();

    private static RestClientMockUp INSTANCE = null;

    public static RestClientMockUp instance() {
        if (INSTANCE == null) {
            INSTANCE = new RestClientMockUp();
        }
        return INSTANCE;
    }

    //================================================================================
    // User requests
    //================================================================================

    @Override
    public void user_add(User.TYPE type, String name, final ResultHandler rh) {
        rh.onResult(RestServerMockUp.instance().user_add(User.TYPE.PATIENT, name), true);
    }

    @Override
    public void user_get(String id, final ResultHandler rh) {
        rh.onResult(RestServerMockUp.instance().user_get(id), true);
    }

    @Override
    public void user_delete(String id, final ResultHandler rh) {
        rh.onResult(RestServerMockUp.instance().user_delete(id), true);
    }

    //================================================================================
    // Measurement requests
    //================================================================================

    @Override
    public void measurement_add(String userId, int measurementGroupId, List<DataPoint> values, final ResultHandler rh) {
        rh.onResult(RestServerMockUp.instance().measurement_add(userId, measurementGroupId, values), true);
    }

    @Override
    public void measurement_append(int measurementId, List<DataPoint> values, final ResultHandler rh) {
        rh.onResult(RestServerMockUp.instance().measurement_append(measurementId, values), true);
    }

    @Override
    public void measurement_get(int id, int index, final ResultHandler rh) {
        rh.onResult(RestServerMockUp.instance().measurement_get(id), true);
    }

    @Override
    public void measurement_delete(int id, final ResultHandler rh) {
        rh.onResult(RestServerMockUp.instance().measurement_delete(id), true);
    }

    @Override
    public void measurement_user_list_get(String userId, final ResultHandler rh) {
        rh.onResult(RestServerMockUp.instance().measurement_user_list_get(userId), true);
    }

    @Override
    public void measurement_group_list_get(int groupId, final ResultHandler rh) {
        rh.onResult(RestServerMockUp.instance().measurement_group_list_get(groupId), true);
    }

    //================================================================================
    // Other requests
    //================================================================================

    @Override
    public void destroyAll(final ResultHandler rh) {
        rh.onResult(RestServerMockUp.instance().destroyAll(), true);
    }

    //================================================================================
    // Test requests
    //================================================================================

    public void test_user_get(final ResultHandler handler) {
        RestRequest.get("users", null, new MaphJsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    JSONObject jsonUser = (JSONObject) response.get(0);
                    User user = new User();
                    user.mId = "" + jsonUser.getInt("id");
                    user.mName = jsonUser.getString("name");
                    handler.onResult(user, true);
                } catch (JSONException e) {
                    e.printStackTrace();
                    handler.onResult(e.getMessage(), false);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                handler.onResult(errorResponse, false);
            }
        });
    }

}
