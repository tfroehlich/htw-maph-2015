package com.htw.students.restapi;

import com.htw.students.common.logger.Log;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Timm Fröhlich on 31.01.2016.
 */
public class MaphJsonHttpResponseHandler extends JsonHttpResponseHandler {

    private static final String TAG = MaphJsonHttpResponseHandler.class.getSimpleName();

    public static boolean mOnSuccessLog = true;
    public static boolean mOnFailureLog = true;
    public static boolean mVerbose = false;

    @Override
    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
        if (mOnSuccessLog)
            onSuccessLog(statusCode, headers, response);
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
        if (mOnSuccessLog)
            onSuccessLog(statusCode, headers, response);
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
        if (mOnFailureLog)
            onFailureLog(statusCode, headers, throwable, errorResponse);
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
        if (mOnFailureLog)
            onFailureLog(statusCode, headers, throwable, errorResponse);
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
        if (mOnFailureLog)
            onFailureLog(statusCode, headers, responseString, throwable);
    }

//================================================================================
// Debugging
//================================================================================

    public static void onSuccessLog(int statusCode, Header[] headers) {
        Log.d(TAG, "StatusCode: " + statusCode);
        if (mVerbose) Log.d(TAG, "headers: " + headers);
    }

    public static void onSuccessLog(int statusCode, Header[] headers, JSONObject response) {
        onSuccessLog(statusCode, headers);
        Log.d(TAG, "JSONObject: \n" + response.toString());
    }

    public static void onSuccessLog(int statusCode, Header[] headers, JSONArray response) {
        onSuccessLog(statusCode, headers);
        Log.d(TAG, "JSONArray: \n" + response.toString());
    }

    public static void onFailureLog(int statusCode, Header[] headers, Throwable throwable) {
//        Log.d(TAG, "StatusCode: " + statusCode);
//        Log.d(TAG, "headers: " + headers);
        Log.d(TAG, "throwable: " + throwable.getMessage());
    }

    public static void onFailureLog(int statusCode, Header[] headers, String responseString, Throwable throwable) {
        onFailureLog(statusCode, headers, throwable);
        Log.d(TAG, "responseString: \n" + responseString);
    }

    public static void onFailureLog(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
        onFailureLog(statusCode, headers, throwable);
        Log.d(TAG, "errorResponse: \n" + errorResponse.toString());
    }

    public static void onFailureLog(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
        onFailureLog(statusCode, headers, throwable);
        Log.d(TAG, "errorResponse: \n" + errorResponse.toString());
    }
}
