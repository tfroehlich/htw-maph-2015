package com.htw.students.model;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.SharedPreferences;

import com.htw.students.common.logger.Log;
import com.htw.students.maph.R;

/**
 * Created by Timm Fröhlich on 07.02.2016.
 *
 * @todo move identifiers to a custom config file
 */
public class Preferences {

    public static final String TAG = Preferences.class.getSimpleName();

    //================================================================================
    // Settings and identifiers
    //================================================================================

    public static int SPLASH_SCREEN_TIMEOUT = 500;
    public static int GRAPH_UPDATE_TASK_SLEEP = 100;
    public static int MEASUREMENT_EXPORT_TIMESPAN = 2000; // in ms
    public static int MEASUREMENT_IMPORT_TIMESPAN = 2000; // in ms
    public static int RANDOM_SERIES_TIMEOUT = 200;

    public static final String USER_ID = "UID";
    public static final String LAST_BT_MAC = "LAST_BT_MAC";
    public static final String USER_RANDOM_SERIES = "USER_RANDOM_SERIES";
    public static final String THRESHOLD = "THRESHOLD";

    //================================================================================
    // Instance
    //================================================================================

    public static SharedPreferences SHARED = null;
    public static SharedPreferences.Editor EDITOR = null;

    public static void initialize(Context context) {
        if (context != null) {
            SHARED = context.getSharedPreferences(
                    context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
            EDITOR = SHARED.edit();

            if (!Preferences.SHARED.contains(USER_RANDOM_SERIES)) {
                // will check for bluetooth support
                setUseRandomSeries(false);
            }

            Log.d(TAG, describe());
        }
    }

    //================================================================================
    // Getter / Setter
    //================================================================================

    public static String currentUserId() {
        return Preferences.SHARED.getString(USER_ID, null);
    }

    public static void setCurrentUserId(String userId) {
        if (userId == null)
            EDITOR.remove(USER_ID);
        else
            EDITOR.putString(USER_ID, userId);
        EDITOR.commit();
    }

    public static String lastBtMacAddress() {
        return Preferences.SHARED.getString(LAST_BT_MAC, null);
    }

    public static void setLastBtMacAddress(String address) {
        if (address == null)
            EDITOR.remove(LAST_BT_MAC);
        else
            EDITOR.putString(LAST_BT_MAC, address);
        EDITOR.commit();
    }

    public static boolean useRandomSeries() {
        return Preferences.SHARED.getBoolean(USER_RANDOM_SERIES, false);
    }

    public static void setUseRandomSeries(boolean useRandomSeries) {
        // always use random series if the device does not support bluetooth
        BluetoothAdapter bt = BluetoothAdapter.getDefaultAdapter();
        if (bt == null) {
            useRandomSeries = true;
        }
        EDITOR.putBoolean(USER_RANDOM_SERIES, useRandomSeries);
        EDITOR.commit();
    }

    public static int threshold() {
        return Preferences.SHARED.getInt(THRESHOLD, 55);
    }

    public static void setThreshold(int threshold) {
        EDITOR.putInt(USER_RANDOM_SERIES, threshold);
        EDITOR.commit();
    }

    //================================================================================
    // Util
    //================================================================================

    public static String describe() {
        StringBuilder sb = new StringBuilder();
        sb.append(TAG + ":\n");
        sb.append("\tcurrentUserId: "); sb.append(currentUserId()); sb.append("\n");
        sb.append("\tlastBtMacAddress: "); sb.append(lastBtMacAddress()); sb.append("\n");
        sb.append("\tuseRandomSeries: "); sb.append(useRandomSeries()); sb.append("\n");
        return sb.toString();
    }
}
