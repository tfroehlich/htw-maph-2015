package com.htw.students.model;

import com.jjoe64.graphview.series.DataPoint;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Timm Fröhlich on 31.01.2016.
 *
 * Public container class for a measurement.
 */
public class Measurement {

    public static Measurement CURRENT = null;
    public static Measurement CURRENT_OBSERVED = null;
    public static boolean CURRENT_EXPORTING = false;

    public int mId;
    public int mGroupId;
    public String mUserId = "";
    public int mMeasurementId;
    public int mIndex;
    public List<DataPoint> mValues = new ArrayList<>();
    public Date mModified = new Date();
    public Date mCreated = new Date();

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName());
        sb.append("[");
        sb.append("id="); sb.append(mId); sb.append(", ");
        sb.append("groupId="); sb.append(mGroupId); sb.append(", ");
        sb.append("userId="); sb.append(mUserId); sb.append(", ");
        sb.append("measurementId="); sb.append(mMeasurementId); sb.append(", ");
        sb.append("index="); sb.append(mIndex); sb.append(", ");
        sb.append("modified="); sb.append(mModified.toString()); sb.append(", ");
        sb.append("created="); sb.append(mCreated.toString()); sb.append(", ");
        sb.append("values="); sb.append(mValues);;
        sb.append("]");
        return sb.toString();
    }
}
