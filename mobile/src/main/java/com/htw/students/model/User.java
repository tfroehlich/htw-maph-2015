package com.htw.students.model;

import java.util.Date;

/**
 * Created by Timm Fröhlich on 31.01.2016.
 *
 * Public container class for a user.
 */
public class User {

    public static User CURRENT = null;
    public static User CURRENT_OBSERVED = null;

    public enum TYPE { PATIENT, THERAPIST }

    public String mId;
    public String mName;
    public TYPE mType = TYPE.PATIENT;
    public Date mDate;

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(User.class.getSimpleName());
        sb.append("[");
        sb.append("id="); sb.append(mId); sb.append(", ");
        sb.append("name="); sb.append(mName); sb.append(", ");
        sb.append("type="); sb.append((mType == TYPE.PATIENT ? "patient" : "therapist"));
        sb.append("]");
        return sb.toString();
    }
}
