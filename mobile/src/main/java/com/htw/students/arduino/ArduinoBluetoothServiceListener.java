package com.htw.students.arduino;

import java.util.ArrayList;

/**
 * Created by adair on 07.12.2015.
 */
public interface ArduinoBluetoothServiceListener {

    public void onArduinoFloatDataReceived(ArrayList<Float> list);

}
