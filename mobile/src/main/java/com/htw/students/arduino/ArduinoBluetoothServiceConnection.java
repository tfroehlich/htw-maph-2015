package com.htw.students.arduino;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import com.htw.students.bluetooth.BluetoothService;
import com.htw.students.bluetooth.Constants;
import com.htw.students.common.logger.Log;

import java.util.ArrayList;

/**
 * Created by adair on 07.12.2015.
 */
public class ArduinoBluetoothServiceConnection implements ServiceConnection {
    private static final String TAG = ArduinoBluetoothServiceConnection.class.getSimpleName();

    private BluetoothService btService = null;
    private StringBuilder recDataString = new StringBuilder();
    private ArduinoBluetoothServiceListener mListener = null;

    public void setServiceListener(ArduinoBluetoothServiceListener listener) {
        mListener = listener;
    }

    int debug_cnt = 0;
    final boolean simpleMode = true;

    private Handler btServiceHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case Constants.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    recDataString.append(new String(readBuf, 0, msg.arg1));

//                    Log.d(TAG, "READ. sb.length: " + recDataString.length() + " " + recDataString);
                    debug_cnt++;
                    final int listLen = 1;
                    try {
                        for (;;) {
                            ArrayList<Float> list = new ArrayList<Float>();
                            int end = ArduinoIO.readFloatDataArray(recDataString, list, listLen);
                            if (end >= 0) {
                                if (mListener != null) {
                                    mListener.onArduinoFloatDataReceived(list);
                                }

                                if (simpleMode) {
                                    recDataString.setLength(0);
                                    break;
                                } else {
                                    recDataString.delete(0, end);
                                    if (debug_cnt > listLen+1) {
                                        Log.d(TAG, debug_cnt + " > " + (listLen + 1) + " -> bad. str: " + recDataString);
                                    }
                                    debug_cnt = 0;
                                }
                            } else {
                                break;
                            }
                        }
                    } catch (Exception e) {
                        Log.d(TAG, "error: " + e.getMessage(), e);
                        recDataString.setLength(0);
                    }

                    // clear when message length exceeds expected data length
                    if (recDataString.length() > 100) {
                        Log.w(TAG, "message exceeds expected data length. " + recDataString.toString());
                        recDataString.setLength(0);
                    }

                    break;
                default:
                    android.util.Log.e(TAG, String.valueOf(msg));
                    break;
            }
            return true;
        }
    });

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        Log.d(TAG, "");
        btService = ((BluetoothService.LocalBinder) service).getService();
        btService.registerHandler(btServiceHandler);
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        Log.d(TAG, "");
        btService = null;
    }

    public void connectToDevice(String macAddress) {
        Log.d(TAG, macAddress);
        if (btService != null) {
            btService.connectToDevice(macAddress);
        }
    }

    public String getConnectedMacAddress(boolean onlyWhenValid) {
        if (btService != null) {
            return btService.getConnectedMacAddress(onlyWhenValid);
        } else {
            Log.w(TAG, "btService == null");
            return null;
        }
    }

    public void closeBtConnection() {
        if (btService != null) {
            btService.closeBtConnection();
        }
    }
}
