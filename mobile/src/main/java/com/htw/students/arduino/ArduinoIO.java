package com.htw.students.arduino;

import com.htw.students.maph.MainActivity;

import java.util.ArrayList;


/**
 * Created by adair on 07.12.2015.
 */
public class ArduinoIO {

    private static final String TAG = MainActivity.class.getSimpleName();

    public static class ArduinoIOException extends Exception {
        public ArduinoIOException(String message) {
            super(message);
        }
    }

    static public int readFloatDataArray(StringBuilder sb, ArrayList<Float> list, int size) throws Exception {
        int start = sb.indexOf("#");
        if (start >= 0) {
            int end = sb.indexOf("~", start+1);
            if (end > start) {
                String[] parts = sb.substring(start+1, end-1).split(";");
                if (size != parts.length) {
                    throw new ArduinoIOException("element count [" + parts.length + "] does not match input size [" + size + "]");
                }
                for (int i = 0; i < parts.length; i++) {
                    list.add(Float.valueOf(parts[i].toString()));
                }
                return end;
            }
        }
        return -1;
    }

    static public int readFloatDataArrayDebug(StringBuilder sb, ArrayList<Float> list) throws Exception {
        return readFloatDataArray(sb, list, 5);
    }

    static public float readFloat(StringBuilder sb) throws Exception {
        return Float.valueOf(sb.toString());
    }

    static public int readInt(StringBuilder sb) throws Exception {
        return Integer.valueOf(sb.toString());
    }
}

