package com.htw.students.maph;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.htw.students.common.logger.Log;
import com.htw.students.model.Measurement;
import com.htw.students.model.Preferences;
import com.htw.students.model.User;
import com.htw.students.restapi.RestClientUsage;
import com.htw.students.restapi.ResultHandler;
import com.htw.students.ui.MeasurementListAdapter;
import com.htw.students.ui.UserMeasurementListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jrosenfeld on 02.02.16.
 */
public class UserMeasurementListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = UserMeasurementListFragment.class.getSimpleName();

    protected View view;
    protected SwipeRefreshLayout refresher;
    protected ListView userMeasurementListView;
    protected List<Integer> measurementIds;
    protected List<Measurement> measurements;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_user_messurement_list, null);
            refresher = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
            refresher.setOnRefreshListener(this);
            userMeasurementListView = (ListView) view.findViewById(R.id.user_measurements);

            userMeasurementListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Log.d(TAG, position + " : " + id);
                    if (position == 0) {
                        Measurement.CURRENT_OBSERVED = null;
                        Toast.makeText(getContext(), "Auswahl aufgehoben", Toast.LENGTH_LONG).show();
                    } else {
                        Measurement.CURRENT_OBSERVED = measurements.get(position);
                        Toast.makeText(getContext(),
                                "Messung ausgewählt: " + Measurement.CURRENT_OBSERVED.mMeasurementId,
                                Toast.LENGTH_LONG).show();
                    }
                }

            });
        }
        return view;
    }

    private void loadData() {
        Log.d(TAG, "------------------------");
        String userId = User.CURRENT_OBSERVED.mId;
        if (userId != null) {
            measurements = new ArrayList<>();
            RestClientUsage.instance().measurement_user_list_get(userId, new ResultHandler() {
                @Override
                public void onResult(Object obj, boolean success) {
                    if (success) {
                        measurementIds = (List<Integer>) obj;
                        handleMeasurementIds(0);
                    }
                }
            });
        } else {
            Log.w(TAG, "userId null");
        }
    }

    private void handleMeasurementIds(final int currentIndex) {
//        Log.w(TAG, "currentIndex: "+ currentIndex);
        if (currentIndex < measurementIds.size()) {
            RestClientUsage.instance().measurement_get(measurementIds.get(currentIndex), 0, new ResultHandler() {
                @Override
                public void onResult(Object obj, boolean success) {
                    if (success) {
                        Measurement m = (Measurement) obj;
                        if (m.mValues.size() > 0) {
                            measurements.add((Measurement) obj);
                        } else {
                            Log.w(TAG, "measurement_get result was empty");
                        }
                        handleMeasurementIds(currentIndex + 1);
                    } else {
                        Log.w(TAG, "measurement_get failed");
                    }
                }
            });
        } else {
            Measurement empty = new Measurement();
            empty.mId = 0;
            measurements.add(0, empty);
            userMeasurementListView.setAdapter(new UserMeasurementListAdapter(measurements));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AppCompatActivity) {
            try {
                ((AppCompatActivity) context).getSupportActionBar().setTitle(R.string.my_measurement_title);
            } catch (NullPointerException ex) {
                Log.e(TAG, ex.getMessage());
            }
        }
        loadData();
    }

    @Override
    public void onRefresh() {
        loadData();
        refresher.setRefreshing(false);
    }
}
