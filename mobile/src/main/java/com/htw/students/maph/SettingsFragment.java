package com.htw.students.maph;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.htw.students.common.logger.Log;
import com.htw.students.model.Preferences;
import com.htw.students.restapi.RestClientUsage;
import com.htw.students.restapi.ResultHandler;


public class SettingsFragment extends Fragment {

    private static final String TAG = SettingsFragment.class.getSimpleName();

    private Context context;
    private View view;

    private Button mBtnDeleteUser;
    private Button mBtnDeleteMac;
    private CheckBox mChbUseRandomSeries;
    private TextView txvUserId;
    private TextView txvMacAddress;
    private EditText etxThreshold;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context != null) {
            this.context = context;
        }

        try {
            ((AppCompatActivity) context).getSupportActionBar().setTitle(R.string.settings);
        } catch (NullPointerException ex) {
            Log.e(TAG, ex.getMessage());
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_settings, null);
        }
        if (view != null) {
            initButtons();
            updateSettingsFields();
        }
        return view;
    }

    private void initButtons() {
        txvUserId = (TextView) view.findViewById(R.id.user_id_tv);
        txvMacAddress = (TextView) view.findViewById(R.id.mac_tv);

        mBtnDeleteUser = (Button) view.findViewById(R.id.btnDeleteUser);
        mBtnDeleteUser.setOnClickListener(new DeleteUserClickListener());

        mBtnDeleteMac = (Button) view.findViewById(R.id.btnDeleteMac);
        mBtnDeleteMac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preferences.setLastBtMacAddress(null);
                updateSettingsFields();
            }
        });

        mChbUseRandomSeries = (CheckBox) view.findViewById(R.id.cboRandomSeries);
        mChbUseRandomSeries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preferences.setUseRandomSeries(((CheckBox) v).isChecked());
                updateSettingsFields();
            }
        });

        etxThreshold = (EditText) view.findViewById(R.id.etxThreshold);
        etxThreshold.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int val = Integer.parseInt(String.valueOf(etxThreshold.getText()));
                Log.d(TAG, ""+val);
//                Preferences.setThreshold(val);
            }
        });
    }

    public void updateSettingsFields() {
        if (txvUserId != null) {
            txvUserId.setText(Preferences.currentUserId());
        }
        if (txvMacAddress != null) {
            Log.d(TAG, Preferences.lastBtMacAddress());
            txvMacAddress.setText(Preferences.lastBtMacAddress());
        }
        if (mChbUseRandomSeries != null) {
            mChbUseRandomSeries.setChecked(Preferences.useRandomSeries());
        }
    }

    private class DeleteUserClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if (context != null) {
                View dialogView = ((Activity) context).getLayoutInflater().inflate(R.layout.dialog_delete_user, null);
                final android.widget.CheckBox formServer = (CheckBox) dialogView.findViewById(R.id.delete_user_from_server_box);

                AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                dialog.setTitle(R.string.dialog_delete_user_title);
                dialog.setView(dialogView);
                dialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String userId = Preferences.currentUserId();
                        if (userId != null && formServer.isChecked()) {
                            RestClientUsage.instance().user_delete(userId, new ResultHandler() {
                                @Override
                                public void onResult(Object obj, boolean success) {
                                    if (!success) {
                                        Toast.makeText(context, R.string.delete_user_from_server_fail, Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        }

                        Preferences.setCurrentUserId(null);
                        updateSettingsFields();
                        Toast.makeText(context, R.string.delete_user_success, Toast.LENGTH_LONG).show();
                    }
                });
                dialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        }
    }

}
