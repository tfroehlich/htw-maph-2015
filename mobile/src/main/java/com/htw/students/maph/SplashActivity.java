package com.htw.students.maph;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.htw.students.common.logger.Log;
import com.htw.students.common.logger.LogWrapper;
import com.htw.students.model.Preferences;
import com.htw.students.model.User;
import com.htw.students.restapi.RestClientUsage;
import com.htw.students.restapi.ResultHandler;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = SplashActivity.class.getSimpleName();

    private View mContentView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LogWrapper.initializeLogging();
        Preferences.initialize(this);

        setContentView(R.layout.activity_splash);
        mContentView = findViewById(R.id.fullscreen_content);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        AsyncTask<Void, Void, Void> waiter = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    Thread.sleep(Preferences.SPLASH_SCREEN_TIMEOUT);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                String userId = Preferences.currentUserId();
                Log.d(TAG, "USERID: " + userId);
                if (userId != null && !userId.isEmpty()) {
                    RestClientUsage.instance().user_get(userId, new ResultHandler() {
                        @Override
                        public void onResult(Object obj, boolean success) {
                            if (success) {
                                User user = (User) obj;
                                User.CURRENT = user;
                                Log.d(TAG, "user_get: " + user);
                                openMenuActivity();
                            } else {
                                Log.w(TAG, "loadData: user_list: failed");
                            }
                        }
                    });
                } else {
                    openRegister();
                }
            }
        };
        waiter.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void openMenuActivity() {
        Log.d(TAG, "---");
        Intent intent = new Intent(SplashActivity.this, MenuActivity.class);
        startActivity(intent);
        SplashActivity.this.finish();
    }

    private void openRegister() {
        Intent intent = new Intent(SplashActivity.this, RegisterActivity.class);
        SplashActivity.this.startActivity(intent);
        SplashActivity.this.finish();
    }

}
