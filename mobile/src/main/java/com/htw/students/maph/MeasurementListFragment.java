package com.htw.students.maph;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.htw.students.common.logger.Log;
import com.htw.students.model.Measurement;
import com.htw.students.model.Preferences;
import com.htw.students.restapi.RestClientUsage;
import com.htw.students.restapi.ResultHandler;
import com.htw.students.ui.MeasurementListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jrosenfeld on 02.02.16.
 */
public class MeasurementListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = MeasurementListFragment.class.getSimpleName();

    protected View view;
    protected SwipeRefreshLayout refresher;
    protected ListView measurementListView;
    protected List<Integer> measurementIds;
    protected List<Measurement> measurements;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_messurement_list, null);
            refresher = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
            refresher.setOnRefreshListener(this);
            measurementListView = (ListView) view.findViewById(R.id.measurements);
        }
        return view;
    }

    private void loadData() {
        String userId = Preferences.currentUserId();
        if (userId != null) {
            measurements = new ArrayList<>();
            RestClientUsage.instance().measurement_user_list_get(userId, new ResultHandler() {
                @Override
                public void onResult(Object obj, boolean success) {
                    if (success) {
                        measurementIds = (List<Integer>) obj;
                        handleMeasurementIds(0);
                    }
                }
            });
        } else {
            Log.w(TAG, "userId null");
        }
    }

    private void handleMeasurementIds(final int currentIndex) {
//        Log.w(TAG, "currentIndex: "+ currentIndex);
        if (currentIndex < measurementIds.size()) {
            RestClientUsage.instance().measurement_get(measurementIds.get(currentIndex), 0, new ResultHandler() {
                @Override
                public void onResult(Object obj, boolean success) {
                    if (success) {
                        Measurement m = (Measurement) obj;
                        if (m.mValues.size() > 0) {
                            measurements.add((Measurement) obj);
                        } else {
                            Log.w(TAG, "measurement_get result was empty");
                        }
                        handleMeasurementIds(currentIndex + 1);
                    } else {
                        Log.w(TAG, "measurement_get failed");
                    }
                }
            });
        } else {
            measurementListView.setAdapter(new MeasurementListAdapter(measurements));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AppCompatActivity) {
            try {
                ((AppCompatActivity) context).getSupportActionBar().setTitle(R.string.my_measurement_title);
            } catch (NullPointerException ex) {
                Log.e(TAG, ex.getMessage());
            }
        }
        loadData();
    }

    @Override
    public void onRefresh() {
        loadData();
        refresher.setRefreshing(false);
    }
}
