package com.htw.students.maph;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.htw.students.common.logger.Log;
import com.htw.students.model.Preferences;
import com.htw.students.model.User;
import com.htw.students.restapi.RestClientUsage;
import com.htw.students.restapi.ResultHandler;

public class RegisterActivity extends AppCompatActivity {

    private String TAG = MenuActivity.class.getSimpleName();

    EditText pseudoInput;
    Spinner chooseTypeView;
    private LinearLayout pseudoInputLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        pseudoInputLayout = (LinearLayout) findViewById(R.id.input_pseudo_layout);
        pseudoInput = (EditText) findViewById(R.id.input_pseudo);
        chooseTypeView = (Spinner) findViewById(R.id.input_user_type);

        String pleaseChooseText = getResources().getString(R.string.please_choose);
        String patientText = getResources().getString(R.string.patient);
        String therapeutText = getResources().getString(R.string.therapeut);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, new String[]{pleaseChooseText, patientText, therapeutText});

        chooseTypeView.setAdapter(adapter);
    }

    public void register(View view) {
        User.TYPE type = null;
        boolean hasError = false;

        String name = pseudoInput.getText().toString();
        if (TextUtils.isEmpty(name) && pseudoInputLayout.getVisibility() == View.VISIBLE) {
            pseudoInput.setError("Bitte gib einen Namen ein!");
            hasError = true;
        }

        int selectedItem = chooseTypeView.getSelectedItemPosition();
        switch (selectedItem) {
            case 0:
                TextView selectedView = (TextView) chooseTypeView.getSelectedView();
                String errorText = "Bitte wähle ein Typ!";
                selectedView.setError(errorText);
                selectedView.setTextColor(Color.RED);
                selectedView.setText(errorText);
                hasError = true;
                break;
            case 1:
                type = User.TYPE.PATIENT;
                break;
            case 2:
                type = User.TYPE.THERAPIST;
                break;
        }

        if (!hasError) {
            RestClientUsage.instance().user_add(type, name, new ResultHandler() {
                @Override
                public void onResult(Object obj, boolean success) {
                    if (success) {
                        if (obj instanceof User) {
                            User user = (User) obj;
                            Log.d(TAG, user.toString());
                            Preferences.setCurrentUserId(user.mId);
                            User.CURRENT = user;

                            Intent intent = new Intent(RegisterActivity.this, MenuActivity.class);
                            intent.putExtra(Preferences.USER_ID, user.mId);
                            startActivity(intent);
                        }
                    } else {
                        Toast.makeText(RegisterActivity.this, obj.toString(), Toast.LENGTH_LONG).show();
                    }
                    super.onResult(obj, success);
                }
            });
        }
    }
}
