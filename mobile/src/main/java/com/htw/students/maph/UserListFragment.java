package com.htw.students.maph;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.htw.students.common.logger.Log;
import com.htw.students.model.Preferences;
import com.htw.students.model.User;
import com.htw.students.restapi.RestClientUsage;
import com.htw.students.restapi.ResultHandler;
import com.htw.students.ui.UserListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jrosenfeld on 02.02.16.
 */
public class UserListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = UserListFragment.class.getSimpleName();

    protected View view;
    protected SwipeRefreshLayout refresher;
    protected ListView userListView;
    protected UserMeasurementListFragment userMeasurementListFragment;
    protected List<User> users;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_users_list, null);
            refresher = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
            refresher.setOnRefreshListener(this);
            userListView = (ListView) view.findViewById(R.id.users);

            userMeasurementListFragment = new UserMeasurementListFragment();

            userListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Log.d(TAG, position + " : " + id);
                    if (position == 0) {
                        User.CURRENT_OBSERVED = null;
                        Toast.makeText(getContext(), "Auswahl aufgehoben", Toast.LENGTH_LONG).show();
                    } else {
                        User.CURRENT_OBSERVED = users.get(position);
                        Toast.makeText(getContext(), "Patient ausgewählt: " + User.CURRENT_OBSERVED.mName, Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
        return view;
    }

    private void loadData() {
        Log.d(TAG, "------------------------");
        String userId = Preferences.currentUserId();
        if (userId != null) {
            users = new ArrayList<>();
            RestClientUsage.instance().user_list(new ResultHandler() {
                @Override
                public void onResult(Object obj, boolean success) {
                    if (success) {
                        users = (List<User>) obj;

//                        if (User.CURRENT_OBSERVED != null) {
                            User empty = new User();
                            empty.mId = "Auswahl aufheben";
                            empty.mName = User.CURRENT_OBSERVED != null
                                    ? "(" +  User.CURRENT_OBSERVED.mName + ")"
                                    : "";
                            users.add(0, empty);
//                        }

                        userListView.setAdapter(new UserListAdapter(users));
                    } else {
                        Log.w(TAG, "loadData: user_list: failed");
                    }
                }
            });
        } else {
            Log.w(TAG, "userId null");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AppCompatActivity) {
            try {
                ((AppCompatActivity) context).getSupportActionBar().setTitle(R.string.users_list);
            } catch (NullPointerException ex) {
                Log.e(TAG, ex.getMessage());
            }
        }
        loadData();
    }

    @Override
    public void onRefresh() {
        loadData();
        refresher.setRefreshing(false);
    }
}
