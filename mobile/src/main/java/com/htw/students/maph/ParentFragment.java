package com.htw.students.maph;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import com.htw.students.arduino.ArduinoBluetoothServiceConnection;
import com.htw.students.arduino.ArduinoBluetoothServiceListener;
import com.htw.students.bluetooth.BluetoothService;
import com.htw.students.bluetooth.DeviceListActivity;
import com.htw.students.common.logger.Log;
import com.htw.students.common.logger.LogWrapper;
import com.htw.students.model.Preferences;

import java.util.ArrayList;
import java.util.Date;


public class ParentFragment extends Fragment implements ArduinoBluetoothServiceListener {

    private static final String TAG = ParentFragment.class.getSimpleName();

    protected BluetoothAdapter mBluetoothAdapter = null;
    protected Intent mServiceIntent = null;
    protected ArduinoBluetoothServiceConnection mServiceConnection = new ArduinoBluetoothServiceConnection();

    protected boolean mArduinoRunning = false;

    // Intent request codes
    protected static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    protected static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    protected static final int REQUEST_ENABLE_BT = 3;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(getActivity(), "Bluetooth is not available", Toast.LENGTH_LONG).show();
//            this.finish();
        }
//        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//        StrictMode.setThreadPolicy(policy);
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        LogWrapper.initializeLogging();

        // request to enable bluetooth
        if (mBluetoothAdapter != null && !mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        mServiceConnection.setServiceListener(this);
        mServiceIntent = new Intent(getActivity(), BluetoothService.class);
        getActivity().bindService(mServiceIntent, mServiceConnection, Context.BIND_AUTO_CREATE);

        String macAddress = mServiceConnection.getConnectedMacAddress(false);
        if (macAddress != null) {
            Preferences.setLastBtMacAddress(macAddress);
            Log.d(TAG, "current mac adress: " + Preferences.lastBtMacAddress());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
//        stopService(mServiceIntent);
//        unbindService(mServiceConnection);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected");
        switch (item.getItemId()) {
            case R.id.secure_connect_scan: {
                Log.d(TAG, "secure_connect_scan");
                // Launch the DeviceListActivity to see devices and do scan
                Intent serverIntent = new Intent(getActivity(), DeviceListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
                return true;
            }
//            case R.id.discoverable: {
//                // Ensure this device is discoverable by others
//                ensureDiscoverable();
//                return true;
//            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectArduino(data);
                }
                break;
            case REQUEST_ENABLE_BT:
                if (resultCode != Activity.RESULT_OK) {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(TAG, "BT not enabled");
                    Toast.makeText(getActivity(), R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                }
        }
    }

    public void connectArduino(Intent data) {
        String macAddress = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        connectBt(macAddress);
    }

    protected void connectBt(String macAddress) {
        Log.d(TAG, "macAddress: " + macAddress);
        mServiceConnection.connectToDevice(macAddress);
    }

    public void onArduinoFloatDataReceived(ArrayList<Float> list) {
        Date date = new Date();
        double d = list.get(0);
        Log.d(TAG, date.getTime() + ": " + d /*list.toString()*/);
    }

}
