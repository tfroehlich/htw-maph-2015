package com.htw.students.maph;


import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.htw.students.common.logger.Log;
import com.htw.students.model.Preferences;
import com.htw.students.model.User;
import com.htw.students.restapi.RestClientUsage;
import com.htw.students.restapi.ResultHandler;
import com.htw.students.ui.MenuListAdapter;

import java.util.List;

public class MenuActivity extends AppCompatActivity {

    private String TAG = MenuActivity.class.getSimpleName();

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ListView mDrawerList;

    //Fragments
    private GroupFragment groupFragment;
    private MeasurementFragment measurementFragment;
    private MeasurementListFragment measurementListFragment;
    private UserListFragment userListFragment;
    private UserMeasurementListFragment userMeasurementListFragment;
    private SettingsFragment settingsFragment;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "###");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_abstract_menu);

        measurementFragment = new MeasurementFragment();
        measurementListFragment = new MeasurementListFragment();
        userListFragment = new UserListFragment();
        userMeasurementListFragment = new UserMeasurementListFragment();
        groupFragment = new GroupFragment();
        settingsFragment = new SettingsFragment();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                mDrawerList.bringToFront();
                mDrawerLayout.requestLayout();
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        Log.d(TAG, "###");
        setupDrawerList();
        setupToolbar();

//        openMeasurementFragment();
        handleStartupFragment();
    }

    private void handleStartupFragment() {
        Log.d(TAG, "??? " + User.CURRENT);
        String userId = null;
        if (User.CURRENT != null) {
            userId = User.CURRENT.mId;
            Log.d(TAG, "userId from User.CURRENT: " + User.CURRENT.mId + " " + User.CURRENT.mName);

            if (User.CURRENT.mType == User.TYPE.THERAPIST) {
                Log.d(TAG, "openUserListFragment");
                openUserListFragment();
                return;
            }
        } else {
            userId = Preferences.currentUserId();
            Log.d(TAG, "??? userId " + Preferences.currentUserId());
        }
        if (userId == null) {
            Log.d(TAG, "no user??");
        }
        RestClientUsage.instance().measurement_user_list_get(userId, new ResultHandler() {
            @Override
            public void onResult(Object obj, boolean success) {
                if (success) {
                    Log.d(TAG, "measurement_user_list_get");
                    List<Integer> list = (List<Integer>) obj;
                    if (list != null) {
                        Log.d(TAG, "measurement_user_list_get: " + list);
                        if (list.size() > 0) {
                            openMeasurementListFragment();
                            return;
                        }
                    } else {
                        Log.w(TAG, "measurement_user_list_get: : null");
                        openMeasurementFragment();
                    }
                } else {
                    Log.w(TAG, "measurement_user_list_get failed");
                    openMeasurementFragment();
                }
            }
        });
    }

    public void openMeasurementListFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, measurementListFragment).commit();
    }

    public void openMeasurementFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, measurementFragment).commit();
    }

    public void openUserListFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, userListFragment).commit();
    }

    public void openUserMeasureListFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, userMeasurementListFragment).commit();
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    private void setupDrawerList() {
        mDrawerList = (ListView) findViewById(R.id.navList);
        mDrawerList.setAdapter(new MenuListAdapter());
        mDrawerList.setDivider(null);
        mDrawerList.setClickable(true);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, position + " : " + id);
                mDrawerList.setItemChecked(position, true);
                mDrawerList.setSelection(position);
                if (position == 0) {
                    return;
                }

                Fragment newFragment = fragmentForPosition(position);
                if (newFragment != null) {
                    FragmentManager manager = getSupportFragmentManager();
                    Fragment fragment = manager.findFragmentById(R.id.fragment);
                    if (fragment == null || !newFragment.getClass().isInstance(fragment)) {
                        Log.d(TAG, "onItemClick: " + newFragment);
                        manager.beginTransaction().replace(R.id.fragment, newFragment).addToBackStack(null).commit();
                    }
                }

                mDrawerLayout.closeDrawers();
            }
        });
    }

    @Nullable
    private Fragment fragmentForPosition(int position) {
        switch (position) {
            case 1: return measurementFragment;
            case 2: return measurementListFragment;
            case 3: return userListFragment;
            case 4:
                if (User.CURRENT_OBSERVED == null) {
                    Toast.makeText(this, "Wähle zuerst einen Patienten aus", Toast.LENGTH_LONG).show();
                    return null;
                } else {
                    return userMeasurementListFragment;
                }
            case 5: return groupFragment;
            case 6: return settingsFragment;
        }
        return null;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
}
