package com.htw.students.maph;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import com.htw.students.util.MeasurementAdapter;


public class GroupActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);

        ListView list = (ListView) findViewById(android.R.id.list);
        list.setAdapter(new MeasurementAdapter(20));
    }

}
