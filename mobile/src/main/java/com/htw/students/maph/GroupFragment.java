package com.htw.students.maph;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.htw.students.util.MeasurementAdapter;


public class GroupFragment extends Fragment {
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.activity_group, container, false);
            ListView list = (ListView) view.findViewById(android.R.id.list);
            list.setAdapter(new MeasurementAdapter(20));
        }
        return view;
    }
}
