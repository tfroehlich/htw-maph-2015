package com.htw.students.maph;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.htw.students.bluetooth.DeviceListActivity;
import com.htw.students.common.logger.Log;
import com.htw.students.model.Measurement;
import com.htw.students.model.Preferences;
import com.htw.students.restapi.MaphJsonHttpResponseHandler;
import com.htw.students.restapi.RestClientUsage;
import com.htw.students.restapi.RestUserLifeCycle;
import com.htw.students.restapi.ResultHandler;
import com.htw.students.restapi.mockup.MockUpUserLifeCycle;
import com.htw.students.ui.GraphHandler;
import com.htw.students.util.Serialize;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class MeasurementFragment extends ParentFragment {

    private static final String TAG = MainActivity.class.getSimpleName();
    private View view;
    private SurfaceView video;
    private GraphView mGraphView;
    private GraphHandler mGraphHandler;

    private Button mPreferencesButton;
    private Button mShareButton;
    private Button mCaptureButton;
    private Button mCameraButton;
    private Button mSaveButton;

    private final boolean mUseMockUp = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_main, container, false);

        video = (SurfaceView) view.findViewById(R.id.video_view);
        mGraphView = (GraphView) view.findViewById(R.id.graph);
        mGraphHandler = new GraphHandler(mGraphView, "MainGraph");

        bindButtons();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mGraphHandler.resume();
        updateCaptureButton(view.findViewById(R.id.btnCapture));
        hideButtons(Measurement.CURRENT_OBSERVED != null);
        // run api tests
//        run_apiTests(200);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onPause() {
        mGraphHandler.pause();
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AppCompatActivity) {
            try {
                ((AppCompatActivity) context).getSupportActionBar().setTitle(R.string.new_measurement_title);
            } catch (NullPointerException ex) {
                Log.w(TAG, ex.getMessage());
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 23) {
            for (int i = 0; i < permissions.length; i++) {
                if (permissions[i] == Manifest.permission.CAMERA && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    toggleCamera(null);
                }
            }
        }
    }

    //================================================================================
    // Camera
    //================================================================================

    public void toggleCamera(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding

                requestPermissions(new String[]{Manifest.permission.CAMERA}, 23);
                return;
            }
            //public void onRequestPermissionsResult(int requestCode, String[] permissions,int[] grantResults);
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
        }

        Camera cam = Camera.open(0);
        try {
            cam.setDisplayOrientation(90);
            cam.setPreviewDisplay(video.getHolder());
            cam.startPreview();
            video.setVisibility(View.VISIBLE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //================================================================================
    // Arduino
    //================================================================================

    @Override
    public void connectArduino(Intent data) {
        mGraphHandler.startCapture();
        super.connectArduino(data);
    }

    @Override
    public void onArduinoFloatDataReceived(ArrayList<Float> list) {
        final float d = list.get(0);
        mGraphHandler.appendData(d);
    }

    //================================================================================
    // measurements & graph
    //================================================================================

    protected long mLastPlayedSound = 0;

    public void playSound() {
        final long now = System.currentTimeMillis();
        if (now > 2000 + mLastPlayedSound) {
            mLastPlayedSound = now;
            try {
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getActivity().getApplicationContext(), notification);
                r.play();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void updateCaptureButton(View view) {
        Button btn = (Button) view;
        if (btn == null) return;
        if (mArduinoRunning || mGraphHandler.randomSeriesRunning()) {
            btn.setText("Stop");
        } else {
            btn.setText("Start");
        }
    }

    public void hideButtons(boolean hide) {
        if (hide) {
            mPreferencesButton.setVisibility(View.GONE);
            mShareButton.setVisibility(View.GONE);
//            mCaptureButton.setVisibility(View.GONE);
            mCameraButton.setVisibility(View.GONE);
            mSaveButton.setVisibility(View.GONE);
        } else {
            mPreferencesButton.setVisibility(View.VISIBLE);
            mShareButton.setVisibility(View.VISIBLE);
            mCaptureButton.setVisibility(View.VISIBLE);
            mCameraButton.setVisibility(View.VISIBLE);
            mSaveButton.setVisibility(View.VISIBLE);
        }
    }

    public void handleMeasurement() {
        if (Preferences.useRandomSeries()) {
            mGraphHandler.toggleRandomSeries();
        } else {
            if (mArduinoRunning) {
                mServiceConnection.closeBtConnection();
                mArduinoRunning = false;
                mGraphHandler.setupDataSeries(false);

            } else {
                mArduinoRunning = true;
                mGraphHandler.startCapture();
                String macAddress = Preferences.lastBtMacAddress();
                if (macAddress != null) {
                    connectBt(macAddress);
                } else {
                    Intent serverIntent = new Intent(getActivity(), DeviceListActivity.class);
                    startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
                }
            }
        }
        updateCaptureButton(mCaptureButton);
    }

    public void createNewMeasurement() {
        List<DataPoint> list = new ArrayList<>();
        RestClientUsage.instance().measurement_add(Preferences.currentUserId(), 0, list, new ResultHandler() {
            @Override
            public void onResult(Object obj, boolean success) {
                if (success) {
                    Measurement measurement = (Measurement) obj;
                    if (measurement != null) {
                        Log.d(TAG, "createMeasurement measurement: " + measurement);
                        Measurement.CURRENT = measurement;
                        Measurement.CURRENT_EXPORTING = false;
                        handleMeasurement();
                    } else {
                        Log.w(TAG, "createMeasurement: : null");
                        Toast.makeText(getActivity(), "createMeasurement: : null", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Log.w(TAG, "createMeasurement failed");
                    Toast.makeText(getActivity(), "createMeasurement failed", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private static int mIndex;
    private final Handler mHandler = new Handler();
    private Runnable mTimerObserve;

    public boolean observeRunning() {
        return mTimerObserve != null;
    }

    public void startObserve(boolean reset) {
        mGraphHandler.startCapture();

        mTimerObserve = new Runnable() {
            @Override
            public void run() {
                RestClientUsage.instance().measurement_get(
                        Measurement.CURRENT_OBSERVED.mMeasurementId, mIndex, new ResultHandler() {
                            @Override
                            public void onResult(Object obj, boolean success) {
                                if (success) {
                                    Measurement measurement = (Measurement) obj;
                                    if (measurement != null) {
                                        Log.d(TAG, "observe measurement: " + measurement);
                                        mIndex = measurement.mIndex+1;
                                        for (DataPoint dp : measurement.mValues) {
                                            mGraphHandler.appendTimeData(dp.getX(), dp.getY());
                                        }
                                    }
                                } else {
                                    Log.w(TAG, "observe failed");
                                    Toast.makeText(getActivity(), "observe failed", Toast.LENGTH_LONG).show();
                                }
                                mHandler.postDelayed(mTimerObserve, Preferences.MEASUREMENT_IMPORT_TIMESPAN);
                            }
                        });
            }
        };
        mHandler.postDelayed(mTimerObserve, 0);
    }

    public void stopObserve() {
        mHandler.removeCallbacks(mTimerObserve);
        mTimerObserve = null;
    }

    public boolean toggleObserve() {
        if (!observeRunning()) {
            mIndex = 0;
            mGraphHandler.setupDataSeries(false);
            startObserve(true);
        } else {
            stopObserve();
            mGraphHandler.setupDataSeries(false);
        }
        return observeRunning();
    }


    //================================================================================
    // button actions
    //================================================================================

    private void bindButtons() {
        mPreferencesButton = (Button) view.findViewById(R.id.btnPreferences);
        mPreferencesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "not implemented");
                playSound();
            }
        });

        mShareButton = (Button) view.findViewById(R.id.btnShare);
        mShareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                RestClientUsage.instance().test_user_get();
                run_apiTests(0);
            }
        });

        mCaptureButton = (Button) view.findViewById(R.id.btnCapture);
        mCaptureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaphJsonHttpResponseHandler.mOnSuccessLog = false;
                if (Measurement.CURRENT_OBSERVED != null) {
                    toggleObserve();
                } else {
                    createNewMeasurement();
                }
            }
        });

        mCameraButton = (Button) view.findViewById(R.id.btnCamera);
        mCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleCamera(view);
            }
        });

        mSaveButton = (Button) view.findViewById(R.id.btnSave);
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "not implemented");
            }
        });
    }

    //================================================================================
    // apiMockUpTests
    //================================================================================

    public void run_apiTests(final int delay) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mUseMockUp)
                    apiMockUpTests();
                else
                    restApiTests();
            }
        }, delay);
    }

    public void apiMockUpTests() {
        Log.d(TAG, "");
        MaphJsonHttpResponseHandler.mOnSuccessLog = false;
        MaphJsonHttpResponseHandler.mOnFailureLog = false;
        MockUpUserLifeCycle.instance(null).start();
    }

    public void restApiTests() {
        Log.d(TAG, "");
        MaphJsonHttpResponseHandler.mOnSuccessLog = false;
        MaphJsonHttpResponseHandler.mOnFailureLog = true;
        RestUserLifeCycle.instance(null).start(true);
    }

}
