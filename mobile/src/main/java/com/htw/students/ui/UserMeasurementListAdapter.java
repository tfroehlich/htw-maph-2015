package com.htw.students.ui;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.htw.students.common.logger.Log;
import com.htw.students.maph.R;
import com.htw.students.model.Measurement;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.List;

/**
 * Created by jrosenfeld on 02.02.16.
 */
public class UserMeasurementListAdapter implements ListAdapter {

    private static final String TAG = UserMeasurementListAdapter.class.getSimpleName();

    private TextView txtMeasurementId;
    private TextView txtCreated;
    private final List<Measurement> measurements;

    public UserMeasurementListAdapter(List<Measurement> measurements) {
        this.measurements = measurements;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getCount() {
        return measurements == null ? 0 : measurements.size();
    }

    @Override
    public Object getItem(int position) {
        if (measurements != null && position < measurements.size()) {
            return measurements.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int layout = R.layout.fragment_user_messurement_group;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getRootView().getContext().
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View thisView = inflater.inflate(layout, parent, false);

            txtMeasurementId = (TextView) thisView.findViewById(R.id.txtMeasurementId);
            txtCreated = (TextView) thisView.findViewById(R.id.txtCreated);

            if (measurements != null && position < measurements.size()) {
                Measurement measurement = measurements.get(position);
                Log.d(TAG, "" + measurement);
                if (measurement.mId == 0) {
                    txtMeasurementId.setText("Auswahl aufheben");
//                    txtCreated.setText("Auswahl aufheben");
                    txtCreated.setText(
                            (Measurement.CURRENT_OBSERVED != null
                            ? "(" + Measurement.CURRENT_OBSERVED.mGroupId + ")"
                            : ""));
                } else {
                    txtMeasurementId.setText(""+measurement.mMeasurementId);
                    txtCreated.setText(measurement.mCreated.toString());
                }
                return thisView;
            }
        }

        return null;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return measurements == null || measurements.size() == 0;
    }
}
