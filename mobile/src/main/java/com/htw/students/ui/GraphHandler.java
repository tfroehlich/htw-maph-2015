package com.htw.students.ui;

import android.os.AsyncTask;
import android.os.Handler;

import com.htw.students.common.logger.Log;
import com.htw.students.model.Measurement;
import com.htw.students.model.Preferences;
import com.htw.students.restapi.RestClientUsage;
import com.htw.students.restapi.ResultHandler;
import com.htw.students.util.SeriesBundle;
import com.htw.students.util.SeriesManager;
import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * Created by adair on 23.01.2016.
 */
public class GraphHandler {

    private static final String TAG = GraphHandler.class.getSimpleName();

    private GraphView mGraphView;
    private Viewport mViewPort;
    private SeriesBundle mSB;
    String mBundleId = null;

    private final Handler mHandler = new Handler();
    private Runnable mTimerRandSeries;

    private UpdateTask mUpdateTask = null;

    //================================================================================
    // Construction
    //================================================================================

    public GraphHandler(GraphView graphView, String identifier) {
        mGraphView = graphView;
        mViewPort = graphView.getViewport();
        mBundleId = identifier;
        setup();
    }

    protected void setup() {
        Log.d(TAG, "setup");
        setupDataSeries(true);
        setupTimeGraphStyle();
    }

    private void setupTimeGraphStyle() {
        setupTimeGraphStyle(mGraphView, mSB);
    }

    public static void setupTimeGraphStyle(GraphView graphView, SeriesBundle sb) {
        Viewport viewPort = graphView.getViewport();

        viewPort.setXAxisBoundsManual(sb.xAxisBoundsManual);
        viewPort.setYAxisBoundsManual(sb.yAxisBoundsManual);
        viewPort.setMinY(sb.minY);
        viewPort.setMaxY(sb.maxY);
        viewPort.setMinX(sb.minX);
        viewPort.setMaxX(sb.maxX);

        setTimeGraphLabelFormatter(graphView);
    }

    public static void setTimeGraphLabelFormatter(GraphView graphView) {
        graphView.getGridLabelRenderer().setLabelFormatter(
                new DefaultLabelFormatter() {
                    @Override
                    public String formatLabel(double value, boolean isValueX) {
                        if (isValueX) {
                            return "" + (value > 0 ? (int) (value / 1000) : 0);
                        }
                        return super.formatLabel(value, isValueX);
                    }
                }
        );
    }

    public void setupDataSeries(boolean restoreGraph) {
        if (mSB == null) {
            mSB = SeriesManager.defaultManager().getAddSeriesBundle(mBundleId);
        }

        if (restoreGraph) {
            Viewport vp = mGraphView.getViewport();
            vp.setMinY(mSB.minY);
            vp.setMaxY(mSB.maxY);
            vp.setMinX(mSB.minX);
            vp.setMaxX(mSB.maxX);
        } else {
            mSB.resetData();
        }

//        mGraphView.removeSeries(mSB.mSeries);
        mGraphView.addSeries(mSB.mSeries);

        if (restoreGraph) {
            if (mSB.randomSeriesWereRunning) {
                startRandSeries(false);
            }
        }
    }

    public static void updateYAxisForTimeGraph(List<DataPoint> list, Viewport vp) {
        vp.setYAxisBoundsManual(true);

        double y, minY = Double.MAX_VALUE, maxY = Double.MIN_VALUE;

        // - in the new values
        for (Iterator<DataPoint> itr = list.iterator(); itr.hasNext(); ) {
            y = itr.next().getY();
            if (maxY < y) maxY = y;
            if (minY > y) minY = y;
        }

        // update y axis bounds
        vp.setMinY(minY);
        vp.setMaxY(maxY);
    }

    public static int startIndexForXAxis(List<DataPoint> list, Viewport vp, int maxTimeSpan) {
        int size = list.size();
        int maxX = (int)list.get(size-1).getX();
        int x;

        vp.setXAxisBoundsManual(true);
        vp.setMinX(-0.01);
        vp.setMaxX(maxX + 0.01);

        for (int i = 0; i < size; i++) {
            x = (int) list.get(i).getX();
            if (x >= maxX - maxTimeSpan) {
                vp.setMinX(x - 0.01);
                Log.d(TAG, ""+ i + " / " + size + " span " + maxTimeSpan + " - " + x);
                return i;
            }
        }
        return 0;
    }

    //================================================================================
    // Life cycle
    //================================================================================

    public void startCapture() {
        stopRandomSeries();
        setupDataSeries(false);
        mSB.mStart = System.currentTimeMillis();
    }

    public void appendData(double value) {
        final double time = System.currentTimeMillis() - mSB.mStart;
        mSB.mUpdateData.add(new DataPoint(time, value));
        Log.d(TAG, (int) time + "/" + value);
    }

    public void appendTimeData(double time, double value) {
        mSB.mUpdateData.add(new DataPoint(time, value));
    }

    public void pause() {
        mSB.randomSeriesWereRunning = randomSeriesRunning();
        stopRandomSeries();
        stopUpdateTask();
        mSB.updateValues(mGraphView);
    }

    public void resume() {
//        resetData();
//        setupDataSeries(true);
        startUpdateTask();
    }

    //================================================================================
    // Random series
    //================================================================================

    public boolean randomSeriesRunning() {
        return mTimerRandSeries != null;
    }

    public void startRandSeries(boolean reset) {
        if (reset) {
            mSB.mStart = System.currentTimeMillis();
        }
        mTimerRandSeries = new Runnable() {
            @Override
            public void run() {
                appendData(new Random().nextDouble() * 100);
                mHandler.postDelayed(this, Preferences.RANDOM_SERIES_TIMEOUT);
//                mHandler.postDelayed(this, new Random().nextInt() % 200 + 100);
            }
        };
        mHandler.postDelayed(mTimerRandSeries, Preferences.RANDOM_SERIES_TIMEOUT);
    }

    public void stopRandomSeries() {
        mHandler.removeCallbacks(mTimerRandSeries);
        mTimerRandSeries = null;
    }

    public boolean toggleRandomSeries() {
        if (!randomSeriesRunning()) {
            setupDataSeries(false);
            startRandSeries(true);
        } else {
            stopRandomSeries();
            setupDataSeries(false);
        }
        return randomSeriesRunning();
    }

    //================================================================================
    // Update task
    //================================================================================

    protected void startUpdateTask() {
        stopUpdateTask();
        if (mUpdateTask == null) {
            Log.d(TAG, "startUpdateTask");
            mUpdateTask = new UpdateTask();
            mUpdateTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    protected void stopUpdateTask() {
        if (mUpdateTask != null) {
            Log.d(TAG, "stopUpdateTask");
            mUpdateTask.cancel(true);
            mUpdateTask = null;
        }
    }

    private class UpdateTask extends AsyncTask<Void, Integer, Void> {

        protected void updateYAxis(ArrayDeque<DataPoint> updateData) {
            double y, minY = Double.MAX_VALUE, maxY = Double.MIN_VALUE;
            // - in old values but with updated mIndex
            for (int i = mSB.mLastUpdateIndex, size = mSB.mSeriesData.size(); i < size; i++) {
                y = mSB.mSeriesData.get(i).getY();
                if (maxY < y) maxY = y;
                if (minY > y) minY = y;
            }
            // - in the new values
            for (Iterator<DataPoint> itr = updateData.iterator(); itr.hasNext(); ) {
                y = itr.next().getY();
                if (maxY < y) maxY = y;
                if (minY > y) minY = y;
            }

            // update y axis bounds
            mViewPort.setMinY(minY);
            mViewPort.setMaxY(maxY);
//            Log.d(TAG, "y: " + (long) minY + " bis " + (long) maxY);
        }

        protected void handleUpdateData(ArrayDeque<DataPoint> updateData) {
            final int updateSize = updateData.size();
            final long now = System.currentTimeMillis();
            final long newerTimeSpan = (now - mSB.mStart) - mSB.mMaxTimeSpan;
            final DataPoint dpLast = updateData.getLast();
//            Log.d(TAG, "" + updateSize);

            // find the mIndex of the oldest data point inside the time span
            for (int i = mSB.mLastUpdateIndex, size = mSB.mSeriesData.size(); i < size; i++) {
                if (mSB.mSeriesData.get(i).getX() > newerTimeSpan) {
//                    Log.d(TAG, "" + mSB.mLastUpdateIndex + " -> " + i);
                    mSB.mLastUpdateIndex = i;
                    break;
                }
            }

            // find min and max values in the new time span
            if (mSB.yAxisBoundsManualUpdate) {
                updateYAxis(updateData);
            }

            // update x axis bounds
            mViewPort.setMinX(newerTimeSpan);
            mViewPort.setMaxX(dpLast.getX());
//            Log.d(TAG, "x: " + (long) newerTimeSpan + " bis " + (long) dpLast.getX());
//            Log.d(TAG, "dif: " + (System.currentTimeMillis() - now));

            publishProgress(updateSize);
        }

        protected Void doInBackground(Void... inrs) {
            Measurement.CURRENT_EXPORTING = false;
            try {
                while (!isCancelled()) {
                    if (mSB.mUpdateData.size() > 0) {
                        handleUpdateData(mSB.mUpdateData);
                    }
                    Thread.sleep(Preferences.GRAPH_UPDATE_TASK_SLEEP);
                }
            } catch (Exception e) {
                Log.w("DEBUG", e.getMessage());
            }
            return null;
        }

        protected void onProgressUpdate(Integer... values) {
            if (!isCancelled()) {
//                final long now = System.currentTimeMillis();

                // appending the graph series will remove data points from the beginning,
                // so calculate the new size while considering discarded and new data points
                final List<DataPoint> series = mSB.mSeriesData;
                final int updateSize = values[0];
                final int newSize = series.size() - mSB.mLastUpdateIndex + updateSize;
//                Log.d(TAG, "update: " + updateSize + ", size: " + mSB.mSeriesData.size() + ", shown size: " + newSize);

                DataPoint dp;
                Iterator<DataPoint> itr;
                int i = 0;
                for (itr = mSB.mUpdateData.iterator(); itr.hasNext() && i < updateSize; i++) {
                    dp = itr.next();
//                    Log.d(TAG, (int) dp.getX() + "/" + (int)dp.getY());
                    series.add(dp);
                    mSB.mSeries.appendData(dp, false, newSize);
                    itr.remove();
                }
//                Log.d(TAG, "dif: " + (System.currentTimeMillis() - now));

                exportSeries(series);
            }
        }
    }

    protected void exportSeries(List<DataPoint> series) {
        if (Measurement.CURRENT == null) {
            return;
        }
        final long now = System.currentTimeMillis();
        if (now < mSB.mLastExport + Preferences.MEASUREMENT_EXPORT_TIMESPAN) {
            return;
        }
        final int size = series.size();
        if (size < 1 || mSB.mLastExportIndex >= size - 1) {
            return;
        }
        Measurement.CURRENT_EXPORTING = true;
        List<DataPoint> list = new ArrayList<>(series.subList(mSB.mLastExportIndex, size - 1));

        RestClientUsage.instance().measurement_append(Measurement.CURRENT.mMeasurementId, list, new ResultHandler() {
            @Override
            public void onResult(Object obj, boolean success) {
                if (success) {
                    if (obj != null) {
                        Log.d(TAG, "appendMeasurement obj: " + obj);
                        Measurement.CURRENT = (Measurement) obj;
                        mSB.mLastExportIndex = size - 1;
                        mSB.mLastExport = System.currentTimeMillis();
                    } else {
                        Log.w(TAG, "appendMeasurement: : null");
                    }
                } else {
                    Log.w(TAG, "appendMeasurement failed");
                }
                Measurement.CURRENT_EXPORTING = false;
            }
        });
    }
}
