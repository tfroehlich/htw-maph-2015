package com.htw.students.ui;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.htw.students.maph.R;
import com.htw.students.model.User;
import com.jjoe64.graphview.GraphView;

import java.util.List;

/**
 * Created by Timm Fröhlich on 02.02.16.
 */
public class UserListAdapter implements ListAdapter {

    private static final String TAG = UserListAdapter.class.getSimpleName();

    private TextView txtTop;
    private TextView txtBotton;
    private final List<User> users;

    public UserListAdapter(List<User> users) {
        this.users = users;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getCount() {
        return users == null ? 0 : users.size();
    }

    @Override
    public Object getItem(int position) {
        if (users != null && position < users.size()) {
            return users.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getRootView().getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            int layout = R.layout.fragment_user_group;
            View thisView = inflater.inflate(layout, parent, false);
            txtTop = (TextView) thisView.findViewById(R.id.txtTop);
            txtBotton = (TextView) thisView.findViewById(R.id.txtBottom);

            if (users != null && position < users.size()) {
                User user = users.get(position);
                txtTop.setText(user.mName);
                txtBotton.setText(user.mId);
                return thisView;
            }
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return users == null || users.size() == 0;
    }
}
