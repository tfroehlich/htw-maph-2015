package com.htw.students.ui;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.htw.students.common.logger.Log;
import com.htw.students.maph.R;

import org.w3c.dom.Text;

/**
 * Created by jrosenfeld on 01.02.16.
 */
public class MenuListAdapter implements ListAdapter {

    private static final String TAG = MenuListAdapter.class.getSimpleName();

    private final static int[] MenuTypes = {R.layout.menu_header,R.layout.menu_entry};

    private final static int[] MenuEntries = {
            R.string.menu_title,
            R.string.new_measurement_title,
            R.string.my_measurement_title,
            R.string.users_list,
            R.string.users_measurement_list,
            R.string.group_title,
            R.string.action_settings
    };

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getCount() {
        return MenuEntries.length;
    }

    @Override
    public Object getItem(int position) {
        if (MenuEntries.length >= position){
            return MenuEntries[position];
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        int layout = getItemViewType(position);
        View thisView = convertView;
        if (convertView == null || layout != Integer.parseInt(((TextView)convertView.findViewById(R.id.menu_entry_hint)).getText().toString()))
        {
            LayoutInflater inflater = (LayoutInflater) parent.getRootView().getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            thisView = inflater.inflate(layout, parent, false);
            TextView hint = (TextView)thisView.findViewById(R.id.menu_entry_hint);
            hint.setText(Integer.toString(layout));
        }
        if (position>0){
            TextView view = (TextView)thisView.findViewById(R.id.menu_entry_title);
            view.setText(MenuEntries[position]);
      }
        return thisView;
    }

    @Override
    public int getItemViewType(int position) {
        return position>0?MenuTypes[1]:MenuTypes[0];
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return position>0;
    }
}
