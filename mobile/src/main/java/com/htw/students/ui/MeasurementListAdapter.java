package com.htw.students.ui;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.htw.students.maph.R;
import com.htw.students.model.Measurement;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.List;

/**
 * Created by jrosenfeld on 02.02.16.
 */
public class MeasurementListAdapter implements ListAdapter {

    private static final String TAG = MeasurementListAdapter.class.getSimpleName();

    private GraphView graph;
    private TextView positionText;
    private final List<Measurement> measurements;

    public MeasurementListAdapter(List<Measurement> measurements) {
        this.measurements = measurements;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getCount() {
        return measurements == null ? 0 : measurements.size();
    }

    @Override
    public Object getItem(int position) {
        if (measurements != null && position < measurements.size()) {
            return measurements.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int layout = R.layout.fragment_messurement_group;

        View thisView = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getRootView().getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            thisView = inflater.inflate(layout, parent, false);

            graph = (GraphView) thisView.findViewById(R.id.messurement_view);
            graph.removeAllSeries();
            positionText = (TextView) thisView.findViewById(R.id.positionText);
        }

        if (measurements != null && position < measurements.size()) {
            Measurement measurement = measurements.get(position);

            positionText.setText(measurement.mModified.toString());

            GraphHandler.setTimeGraphLabelFormatter(graph);
            GraphHandler.updateYAxisForTimeGraph(measurement.mValues, graph.getViewport());
            int startIndex = GraphHandler.startIndexForXAxis(measurement.mValues, graph.getViewport(), 5000);
//            Log.d(TAG, measurement.mValues.toString());

            final int size = measurement.mValues.size();
            final List<DataPoint> values = measurement.mValues.subList(startIndex, size-1);
            LineGraphSeries<DataPoint> list = new LineGraphSeries<>(
                    values.toArray(new DataPoint[values.size()])
            );
            graph.addSeries(list);
//            list.resetData(data);

            return thisView;
        } else {
            return null;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return measurements == null || measurements.size() == 0;
    }
}
