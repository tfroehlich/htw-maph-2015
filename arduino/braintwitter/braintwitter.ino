#include <eHealth.h>

// light LED to see if arduino is working
const int led = 13;

// Array for the sensors values
float sensorValues[4] = {0,0,0,0};
 
// Char used for reading in Serial characters
char inbyte = 0;
 
void setup() {
  // initialise serial communications at 9600 bps:
  Serial.begin(9600);
  
  delay(2000);// Give reader a chance to see the output.  
  
  pinMode(led, OUTPUT);
  digitalWrite(led, HIGH);
}
 
void loop() {  
  readSensors();
  sendAndroidValues();
  //when serial values have been received this will be true
  if (Serial.available() > 0)
  {
    inbyte = Serial.read();
    digitalWrite(led, LOW);
  }
  delay(1000);
}
 
void readSensors()
{
  // read values
  sensorValues[0] = eHealth.getECG();  
  sensorValues[1] = eHealth.getSkinConductance(); 
  sensorValues[2] = eHealth.getSkinConductanceVoltage();
  sensorValues[3] = eHealth.getSkinResistance();  
}

//sends the values from the sensor over serial to BT module
void sendAndroidValues()
 {
  //puts # before the values so our app knows what to do with the data
  Serial.print('#');
  //loop through the values and send them via serial
  for(int k=0; k<4; k++)
  {
    Serial.print((float)sensorValues[k]);
    Serial.print('p');
  }
  Serial.print('~'); //used as an end of transmission character - used in app for string length
  Serial.println();
  delay(10);        //added a delay to eliminate missed transmissions
}

