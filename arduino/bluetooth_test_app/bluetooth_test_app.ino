const int MAX_INT = 32767;

// platform support for using the serial  
enum PLAT {
  UNITY,
  PYTHON,
  ARDUINO_MONITOR,
  PROCESSING_ANDROID
};
int PLATFORM = PROCESSING_ANDROID; // choose which platform you are sending the data from
boolean ACK = true; //sends a message back with the message received
boolean ACK_READY = true; //sends a message back once USB connection is estabelished

//double check where you have your relays and LED conected to
const int led = 13; //LED
const int relay1 = 12; //arduino pin for relay 1
const int relay2 = 4; //arduino pin for relay 1  

// program variables
int deltaMax = 10000;   // 10 s
int deltaMin = 50;     // 200 ms
int deltaDefault = 100;
int delta = deltaDefault; //waiting time for open/close functions (feels like a hit)
boolean programMode = 0;

void setup() {
  pinMode(led, OUTPUT);
  pinMode(relay1, OUTPUT);     
  pinMode(relay2, OUTPUT);  
  Serial.setTimeout(10);  
  Serial.begin(9600); //speed of communications, set this on the program that interfaces with your arduino too
  if (ACK_READY) Serial.println("Ready"); // print "Ready" once
}

int readInput() {
  char inByte = ' ';
    int x;
    switch (PLATFORM) {
      case UNITY:  
        //if you send data through Unity on Serial (USB)
        x = Serial.parseInt(); 
        if (ACK) Serial.println(x);
        break;
     case PYTHON: 
       //if you send data through Python on Serial (USB)
       inByte = Serial.read();
       if (ACK) Serial.println(inByte); //send data back to python
       x = inByte - '0';
       break;
     case ARDUINO_MONITOR:
        //if you send data through Arduino Console, on Serial (USB)
        x = Serial.parseInt(); 
        if (ACK) Serial.println(x);
        break;
      case PROCESSING_ANDROID:
        //if you are sending data from Processing+Android     
        x = Serial.read(); 
        if (ACK) Serial.println(x);
        break;
    }
    return x;
}

void pulse(int relay, int delta) {
  digitalWrite(relay, HIGH);
  //digitalWrite(led, HIGH);
  delay(delta);
  digitalWrite(relay, LOW);
  //digitalWrite(led, LOW);
  delay(delta);
}

void bluetooth_test_app(int x) {
  if (0 < x && x < 100) {
    if (programMode) {
      pulse(relay1, x * 50);
    } else {
      int cnt = x>>2;
      cnt = x;
      for (int i=0; i<cnt; i++) {
        pulse(relay1, delta);
      }
    }
  } else if (x == 200) {
     digitalWrite(relay1, HIGH);
  } else if (200 < x && x <= 210) {
    switch (x) {
      case 201: pulse(relay1, 50); break;
      case 202: pulse(relay1, 100); break;
      case 203: pulse(relay1, 200); break;
      case 204: pulse(relay1, 500); break;
      case 205: pulse(relay1, 1000); break;
      case 206: pulse(relay1, 2000); break;
      case 207: pulse(relay1, 3000); break;
      case 220: programMode = !programMode; break;
      default: break;      
    }
  } else if (210 < x && x <= 250) {
    delta = (x - 210) * 50;
    if(delta < deltaMin)
         delta = deltaMin;
      if(delta > deltaMax)
         delta = deltaMax;
  } else {
    digitalWrite(relay1, LOW);
    digitalWrite(relay2, LOW);
  }
}

void loop_bluetooth_test_app() {
  if(Serial.available() > 0) {
    int x = readInput();
    //digitalWrite(led, !digitalRead(led));
    digitalWrite(led, HIGH);
    bluetooth_test_app(x);    
    digitalWrite(led, LOW);
  }
}

void loop() {
  //pulse(13, 500); pulse(12, 500);
  loop_bluetooth_test_app();
}

