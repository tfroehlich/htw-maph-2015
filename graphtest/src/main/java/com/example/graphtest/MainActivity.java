package com.example.graphtest;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.camera2.CameraManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;

import com.example.graphtest.util.CameraFactory;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.DataPoint;

import java.security.Permissions;
import java.util.Random;


public class MainActivity extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    LineGraphSeries<DataPoint> series;
    private DataRunner task;
    private boolean started;
    private GraphView graph;
    private SurfaceView video;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        graph = (GraphView) findViewById(R.id.graph);
        video = (SurfaceView) findViewById(R.id.video_view);

        series = new LineGraphSeries<DataPoint>();

        graph.addSeries(series);

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.graphtest/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.graphtest/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
    public void onRequestPermissionsResult(int requestCode, String[] permissions,int[] grantResults){
        if (requestCode == 23){
            for (int i = 0; i<permissions.length;i++){
                if (permissions[i] == Manifest.permission.CAMERA &&  grantResults[i] == PackageManager.PERMISSION_GRANTED ){
                    toogleCamera(null);
                }
            }
        }
    }
    public void toogleCamera(View view){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding

                requestPermissions(new String[]{Manifest.permission.CAMERA},23);
                return;
            }
            //public void onRequestPermissionsResult(int requestCode, String[] permissions,int[] grantResults);
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

        }
        video.setVisibility(View.VISIBLE);
        CameraFactory.CameraHandler handler = CameraFactory.getHandler(this);
        handler.startPreview(video);


    }
    public void toogleCapture(View view ){
        if (started != true){
            task = new DataRunner();
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            ((Button)view).setText("Stop");
            started = true;
        }else {
            if (task != null){
                task.doCancel();
                task = null;
                graph.removeAllSeries();
                series = new LineGraphSeries<DataPoint>();
                graph.addSeries(series);
            }
            ((Button)view).setText("Star");
            started = false;
        }

    }

    public void viewGroup(MenuItem item) {
        Intent startIntent = new Intent(this,GroupActivity.class);
        startActivity(startIntent);
    }

    private class DataRunner extends AsyncTask<Void,Void,Void>{
        long lastTime = 0;
        int avg;
        int count;
        boolean canceled = false;
        public  void doCancel(){
            canceled =true;
        }

        @Override
        protected Void doInBackground(Void... inrs) {
            for (int i = 0; i > -1;i++){
                if (canceled){
                    break;
                }
                avg = avg +  new Random().nextInt();
                count = count +1;
                if (System.currentTimeMillis() - lastTime >= 100){
                    lastTime = System.currentTimeMillis();

                final int j = i;
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        series.appendData(new DataPoint(j,avg/count), true, 20000);
                    }
                });
                    avg = 0;
                        count =    1;

                }

            }
            return null;
        }
    }
}
