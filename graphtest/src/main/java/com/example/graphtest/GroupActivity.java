package com.example.graphtest;

import android.app.Activity;
import android.app.ListActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.graphtest.util.MessurementAdapter;
import com.jjoe64.graphview.GraphView;

public class GroupActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);

        ListView list = (ListView) findViewById(android.R.id.list);
        list.setAdapter(new MessurementAdapter(20));
    }

}
