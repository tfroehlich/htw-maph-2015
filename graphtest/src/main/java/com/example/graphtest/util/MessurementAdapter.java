package com.example.graphtest.util;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.graphtest.R;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by jrosenfeld on 21.01.16.
 */
public class MessurementAdapter implements ListAdapter {
    private static final String TAG = "MessurementAdapter";
    ArrayList<DataSetObserver> observers;
    ArrayList<DataPoint> list = new ArrayList<DataPoint>();
    ArrayList<LineGraphSeries<DataPoint>> entries = new ArrayList<LineGraphSeries<DataPoint>>();
    DataRunner[] runners ;

    public MessurementAdapter(int entryCount) {
        runners = new DataRunner[entryCount];
        for (int i = 0; i < entryCount; i++) {
            LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>();
            DataRunner runner = new DataRunner(series, 0, 0);
            runners[i]= runner;
            runner.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            entries.add(series);
        }
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        if (observers == null) {
            observers = new ArrayList<DataSetObserver>();
        }
        observers.add(observer);

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
        if (observers != null) {
            observers.remove(observer);
        }
    }

    @Override
    public int getCount() {
        if (entries == null) {
            return 0;
        }
        return entries.size();
    }

    @Override
    public Object getItem(int position) {
        if (entries == null || position > entries.size()) {
            return null;
        }
        return entries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int layout = R.layout.fragment_messurement_list;
        View thisView = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getRootView().getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            thisView = inflater.inflate(layout, parent, false);
        }

        GraphView graph = (GraphView) thisView.findViewById(R.id.messurement_view);
        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        staticLabelsFormatter.setVerticalLabels(new String[] {"highest","0","lowest"});

        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        TextView positionText = (TextView) thisView.findViewById(R.id.positionText);
        positionText.setText(position + "");
        LineGraphSeries<DataPoint> list = entries.get(position);
        list.setBackgroundColor(Color.CYAN);
        graph.removeAllSeries();

        graph.addSeries(list);
        return thisView;
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.fragment_messurement_list;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        if (entries.size() >= position) {
            return true;
        }
        return false;
    }

    private static class DataRunner extends AsyncTask<Void, Void, DataPoint> {
        private static final String TAG = "DataRunner";
        private static boolean firstRun = false;
        private final LineGraphSeries<DataPoint> list;
        long lastTime = 0;
        int avg = 0;
        int count = 0;
        boolean canceled = false;

        public DataRunner(LineGraphSeries<DataPoint> list, int avg, int count) {
            this.list = list;
            this.avg = avg;
            this.count = count;
        }

        public void doCancel() {
            canceled = true;
        }

        @Override
        protected DataPoint doInBackground(Void... inrs) {

            if (!firstRun) {
                lastTime = System.currentTimeMillis();
                do {
                    if (canceled) {
                        break;
                    }
                } while (System.currentTimeMillis() - lastTime >= 100);
            } else {
                firstRun = true;
            }
            avg = avg + new Random().nextInt();
            count = count + 1;
            return new DataPoint(count - 1, avg / count);


        }

        @Override
        protected void onPostExecute(DataPoint dataPoint) {
            super.onPostExecute(dataPoint);
            list.appendData(dataPoint, false, 100);

            if (!canceled) {

                DataRunner runner = new DataRunner(list, avg, count);
                runner.executeOnExecutor(THREAD_POOL_EXECUTOR);
            }


        }
    }
}
