package com.example.linvor;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by terra_000 on 25.01.2015.
 */
public class BlueTooth {

    ArrayList<BluetoothDevice> devices;
    BluetoothAdapter btAdapter;
    BluetoothDevice btDevice;
    BluetoothSocket btSocket;
    InputStream iStream;
    OutputStream oStream;
    boolean autoConnect = true;

    public BluetoothDevice getDevice() {
        return btDevice;
    }

    public boolean open() {
        Log.d("d", "openBT");

        btAdapter = BluetoothAdapter.getDefaultAdapter();
        if (btAdapter != null) {
            if (!btAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            } else {

                Log.d("d", "getDeviceList");
                devices = new ArrayList<BluetoothDevice>();
                for (BluetoothDevice device : btAdapter.getBondedDevices()) {
                    Log.d("d","- " + device.getName());
                    devices.add(device);
                }

                // btDevice = (BluetoothDevice) devices.get(0);
                for (BluetoothDevice d : devices) {
                    if (d.getName().equals("HC-06") || d.getName().equals("linvor")) {
                        btDevice = d;
                        break;
                    }
                }

                close();

                if (btDevice == null) {
                    Log.e("e", "No bluetooth device found");
                    return false;
                } else {
                    UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

                    try {
                        btSocket = btDevice.createRfcommSocketToServiceRecord(uuid);
                        Log.d("d", "socket " + btSocket);
                        btSocket.connect();
                        boolean c = btSocket.isConnected();
                        Log.d("d", "socket is " + (c ? "" : "not ") + "connected");
                        iStream = btSocket.getInputStream();
                        oStream = btSocket.getOutputStream();
//                        Log.d("d", "iStream " + iStream + " ;; oStream " + oStream);
                        return true;
                    } catch(Exception ex) {
                        Log.e("e", ex.toString());
                    }
                }
            }
        }
        return false;
    }

    public void close() {
        Log.d("d", "closeBT");
        if (btSocket != null) {
            try {
                btSocket.close();
                btSocket = null;
            } catch(IOException ex) {
                Log.e("e", ex.toString());
            }
        }
    }

    public void send(int i) {
        _send(i);
    }

    public void _send(int i) {
        if (autoConnect && (btDevice == null || btSocket == null || !btSocket.isConnected())) {
            try { open(); } catch (Exception ex) { Log.e("e", ex.toString()); }
        }
        Log.d("d", "send (int): " + i);
        if (btDevice != null) {
            Log.d("d", "bondState: " + btDevice.getBondState());
        } else {
            Log.d("e", "no device");
        }
        try {
            oStream.write(i);
        } catch(Exception ex) {
            Log.e("e", ex.toString());
        }
    }
}
