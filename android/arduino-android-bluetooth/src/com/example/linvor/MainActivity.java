package com.example.linvor;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends Activity {

    private static final int REQUEST_ENABLE_BT = 3;

    TextView textview;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final BlueTooth bluetooth = new BlueTooth();

        Button btnOpen = (Button) findViewById(R.id.open);
        Button btnClose = (Button) findViewById(R.id.close);
        Button btnOn = (Button) findViewById(R.id.on);
        Button btnOff = (Button) findViewById(R.id.off);
        Button btnSendSeekerValue = (Button) findViewById(R.id.sendSeekVal);
        textview = (TextView) findViewById(R.id.label);

        final TextView seekVal = (TextView) findViewById(R.id.seekVal);
        final SeekBar seekBar = (SeekBar) findViewById(R.id.seekBar);
        seekVal.setText(String.valueOf(seekBar.getProgress()));

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekVal.setText(String.valueOf(progress));
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


        btnOpen.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (bluetooth.open()) {
                    textview.setText(bluetooth.getDevice().getName() + " - " + bluetooth.getDevice().getAddress());
                } else {
                    textview.setText("Could not connect bluetooth device");
                }
            }
        });
        btnClose.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                bluetooth.close();
            }
        });
        btnOn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                bluetooth.send(200);
            }
        });
        btnOff.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                bluetooth.send(0);
            }
        });
        btnSendSeekerValue.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                bluetooth.send(seekBar.getProgress());
            }
        });

        Button btn50 = (Button) findViewById(R.id.btn50);
        btn50.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                bluetooth.send(201);
            }
        });
        Button btn100 = (Button) findViewById(R.id.btn100);
        btn100.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                bluetooth.send(202);
            }
        });
        Button btn200 = (Button) findViewById(R.id.btn200);
        btn200.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                bluetooth.send(203);
            }
        });
        Button btn500 = (Button) findViewById(R.id.btn500);
        btn500.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                bluetooth.send(204);
            }
        });
        Button btn1000 = (Button) findViewById(R.id.btn1000);
        btn1000.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                bluetooth.send(205);
            }
        });
        Button btn2000 = (Button) findViewById(R.id.btn2000);
        btn2000.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                bluetooth.send(206);
            }
        });

        Button btnMode = (Button) findViewById(R.id.btnMode);
        btnMode.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                bluetooth.send(220);
            }
        });


        final TextView seekVal2 = (TextView) findViewById(R.id.seekVal2);
        final SeekBar seekBar2 = (SeekBar) findViewById(R.id.seekBar2);
        seekVal2.setText(seekBar2Text(seekBar2.getProgress()));

        seekBar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekVal2.setText(seekBar2Text(progress));
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        Button btnSetDelay = (Button) findViewById(R.id.btnSetDelay);
        btnSetDelay.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                bluetooth.send(seekBar2.getProgress()+210);
            }
        });

    }

    public String seekBar2Text(int progress) {
        return progress + " * 50ms = " + (progress * 50) + "ms";
    }

}
